/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.user.controller;

<<<<<<< HEAD
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.ngrinder.AbstractNGrinderTransactionalTest;
import org.ngrinder.common.controller.NGrinderBaseController;
import org.ngrinder.model.Role;
import org.ngrinder.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.ModelMap;

/**
 * Class description.
 * 
 * @author Mavlarn
 * @since
 */
=======
import com.google.common.collect.Lists;
import org.junit.Test;
import org.ngrinder.AbstractNGrinderTransactionalTest;
import org.ngrinder.common.controller.BaseController;
import org.ngrinder.model.Role;
import org.ngrinder.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.ui.ModelMap;

import java.util.Date;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
public class UserControllerTest extends AbstractNGrinderTransactionalTest {

	@Autowired
	private UserController userController;

	/**
	 * Test method for
<<<<<<< HEAD
	 * {@link org.ngrinder.user.controller.UserController#getUserList(org.springframework.ui.ModelMap, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetUserList() {
		Pageable page = new PageRequest(1, 10);

		ModelMap model = new ModelMap();
		userController.getUserList(model, null, page, null);

		model.clear();
		userController.getUserList(model, "ADMIN", page, null);

		model.clear();
		userController.getUserList(model, null, page, "user");
=======
	 * {@link org.ngrinder.user.controller.UserController#getAll(org.springframework.ui.ModelMap, org.ngrinder.model.Role,
	 * org.springframework.data.domain.Pageable, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetAll() {
		Pageable page = new PageRequest(1, 10);

		ModelMap model = new ModelMap();
		userController.getAll(model, null, page, null);

		model.clear();
		userController.getAll(model, Role.ADMIN, page, null);

		model.clear();
		userController.getAll(model, null, page, "user");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	}

	/**
	 * Test method for
<<<<<<< HEAD
	 * {@link org.ngrinder.user.controller.UserController#getUserDetail(org.ngrinder.model.User, org.springframework.ui.ModelMap, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetUserDetail() {
		ModelMap model = new ModelMap();
		userController.getUserDetail(getTestUser(), model, getTestUser().getUserId());
=======
	 * {@link org.ngrinder.user.controller.UserController#getOne(org.ngrinder.model.User,
	 * org.springframework.ui.ModelMap)}
	 * .
	 */
	@Test
	public void testGetOne() {
		ModelMap model = new ModelMap();
		userController.getOne(getTestUser().getUserId(), model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		User user = (User) model.get("user");
		assertThat(user.getId(), is(getTestUser().getId()));
	}

	/**
	 * Test method for
<<<<<<< HEAD
	 * {@link org.ngrinder.user.controller.UserController#saveOrUpdateUserDetail(org.ngrinder.model.User, org.springframework.ui.ModelMap, org.ngrinder.model.User)}
	 * .
	 */
	@Test
	public void testSaveOrUpdateUserDetail() {
=======
	 * {@link org.ngrinder.user.controller.UserController#save(org.ngrinder.model.User,
	 * org.ngrinder.model.User, org.springframework.ui.ModelMap)}
	 * .
	 */
	@Test
	public void testSave() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		// test update
		ModelMap model = new ModelMap();
		User currUser = getTestUser();
		currUser.setUserName("new name");
<<<<<<< HEAD
		userController.saveOrUpdateUserDetail(currUser, model, currUser, null);
		userController.getUserDetail(getTestUser(), model, currUser.getUserId());
=======
		userController.save(currUser, currUser, model);
		userController.getOne(currUser.getUserId(), model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		User user = (User) model.get("user");
		assertThat(user.getUserName(), is("new name"));
		assertThat(user.getPassword(), is(currUser.getPassword()));

		User admin = getAdminUser();
		User temp = new User("temp1", "temp1", "temp1", "temp@nhn.com", Role.USER);
<<<<<<< HEAD
		userController.saveOrUpdateUserDetail(admin, model, temp, null);
		temp = new User("temp2", "temp2", "temp2", "temp@nhn.com", Role.USER);
		userController.saveOrUpdateUserDetail(admin, model, temp, null);
		model.clear();
		userController.saveOrUpdateUserDetail(currUser, model, currUser, "temp1, temp2");
		userController.getUserDetail(getTestUser(), model, currUser.getUserId());
=======
		userController.save(admin, temp, model);
		temp = new User("temp2", "temp2", "temp2", "temp@nhn.com", Role.USER);
		userController.save(admin, temp, model);
		model.clear();
		currUser.setFollowersStr("temp1, temp2");
		userController.save(currUser, currUser, model);
		userController.getOne(currUser.getUserId(), model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		user = (User) model.get("user");
		assertThat(user.getFollowers().size(), is(2));
		assertThat(user.getFollowers().get(0).getUserId(), is("temp1"));
	}

	@Test
<<<<<<< HEAD
	public void testUpdateCurrentUserRole() {
=======
	public void testUpdate() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		// test update the role of current user.
		ModelMap model = new ModelMap();
		User currUser = getTestUser();
		assertThat(currUser.getRole(), is(Role.USER)); // current test user is "USER"

		User updatedUser = new User(currUser.getUserId(), currUser.getUserName(), currUser.getPassword(),
<<<<<<< HEAD
						"temp@nhn.com", currUser.getRole());
		updatedUser.setId(currUser.getId());
		updatedUser.setEmail("test@test.com");
		updatedUser.setRole(Role.ADMIN); // Attempt to modify himself as ADMIN
		userController.saveOrUpdateUserDetail(currUser, model, updatedUser, null);

		userController.getUserDetail(getTestUser(), model, currUser.getUserId());
=======
				"temp@nhn.com", currUser.getRole());
		updatedUser.setId(currUser.getId());
		updatedUser.setEmail("test@test.com");
		updatedUser.setRole(Role.ADMIN); // Attempt to modify himself as ADMIN
		userController.save(currUser, updatedUser, model);

		userController.getOne(currUser.getUserId(), model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		User user = (User) model.get("user");
		assertThat(user.getUserName(), is(currUser.getUserName()));
		assertThat(user.getPassword(), is(currUser.getPassword()));
		assertThat(user.getRole(), is(Role.USER));
	}

	private void saveTestUser(String userId, String userName) {
		User newUser = new User();
		newUser.setUserId(userId);
		newUser.setUserName(userName);
		newUser.setEmail("junoyoon@gmail.com");
		newUser.setCreatedUser(getTestUser());
		newUser.setCreatedDate(new Date());
		newUser.setRole(Role.USER);
		ModelMap model = new ModelMap();
<<<<<<< HEAD
		userController.saveOrUpdateUserDetail(getAdminUser(), model, newUser, null);
=======
		userController.save(getAdminUser(), newUser, model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Test method for
<<<<<<< HEAD
	 * {@link org.ngrinder.user.controller.UserController#deleteUser(org.springframework.ui.ModelMap, java.lang.String)}
=======
	 * {@link org.ngrinder.user.controller.UserController#delete(org.springframework.ui.ModelMap, java.lang.String)}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * .
	 */
	@SuppressWarnings("unchecked")
	@Test
<<<<<<< HEAD
	public void testDeleteUser() {
=======
	public void testDelete() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		ModelMap model = new ModelMap();
		// save new user for test
		saveTestUser("NewUserId1", "NewUserName1");
		saveTestUser("NewUserId2", "NewUserName2");
		saveTestUser("NewUserId3", "NewUserName3");

<<<<<<< HEAD
		Pageable page = new PageRequest(1, 10);

		// search
		userController.getUserList(model, null, page, "NewUserName");
		List<User> userList = (List<User>) model.get("userList");
		assertThat(userList.size(), is(3));

		// test to delete one
		model.clear();
		userController.deleteUser(model, "NewUserId1");
		model.clear();
		userController.getUserList(model, "user", page, "NewUserName");
		userList = (List<User>) model.get("userList");
		assertThat(userList.size(), is(2));

		// test to delete more
		model.clear();
		userController.deleteUser(model, "NewUserId2,NewUserId3");
		model.clear();
		userController.getUserList(model, "user", page, "NewUserName");
		userList = (List<User>) model.get("userList");
		assertThat(userList.size(), is(0));
=======
		Pageable page = new PageRequest(0, 10);

		// search
		userController.getAll(model, null, page, "NewUserName");
		PageImpl userList = (PageImpl<User>) model.get("users");
		assertThat(userList.getContent().size(), is(3));

		// test to delete one
		model.clear();
		userController.delete(testUser, "NewUserId1", model);
		model.clear();
		userController.getAll(model, Role.USER, page, "NewUserName");
		userList = (PageImpl<User>) model.get("users");
		assertThat(userList.getContent().size(), is(2));

		// test to delete more
		model.clear();
		userController.delete(testUser, "NewUserId2,NewUserId3", model);
		model.clear();
		userController.getAll(model, Role.USER, page, "NewUserName");
		userList = (PageImpl<User>) model.get("users");
		assertThat(userList.getContent().size(), is(0));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Test method for
<<<<<<< HEAD
	 * {@link org.ngrinder.user.controller.UserController#checkUserId(org.springframework.ui.ModelMap, java.lang.String)}
	 * .
	 */
	@Test
	public void testCheckUserId() {
		NGrinderBaseController ngridnerBaseController = new NGrinderBaseController();
		ModelMap model = new ModelMap();
		String rtnStr = userController.checkUserId(model, "not-exist");
		assertThat(rtnStr, is(ngridnerBaseController.returnSuccess()));

		rtnStr = userController.checkUserId(model, getTestUser().getUserId());
		assertThat(rtnStr, is(ngridnerBaseController.returnError()));
	}

	@Test
	public void testUserProfile() {
		ModelMap model = new ModelMap();
		String viewName = userController.userProfile(getTestUser(), model);
=======
	 * {@link UserController#checkDuplication(String)}
	 * .
	 */
	@Test
	public void testDuplication() {
		BaseController ngrinderBaseController = new BaseController();
		HttpEntity<String> rtnStr = userController.checkDuplication("not-exist");
		assertThat(rtnStr.getBody(), is(ngrinderBaseController.returnSuccess()));

		rtnStr = userController.checkDuplication(getTestUser().getUserId());
		assertThat(rtnStr.getBody(), is(ngrinderBaseController.returnError()));
	}

	@Test
	public void testProfile() {
		ModelMap model = new ModelMap();
		String viewName = userController.getOne(getTestUser(), model);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		assertThat(viewName, is("user/info"));
	}

	@Test
	public void testSwitchOptions() {
		ModelMap model = new ModelMap();
<<<<<<< HEAD
		userController.switchOptions(getTestUser(), model);

		assertThat(model.containsAttribute("shareUserList"), is(true));
=======
		User currUser = getTestUser();
		User temp = new User("temp1", "temp1", "temp1", "temp@nhn.com", Role.USER);
		User admin = getAdminUser();
		userController.save(admin, temp, model);
		currUser.setOwners(Lists.newArrayList(temp));
		currUser.setOwnerUser(temp);
		userController.save(currUser, currUser, model);
		HttpEntity<String> shareUsersStr = userController.switchOptions(currUser, "");
		assertTrue(shareUsersStr.getBody().contains("id"));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}
}
