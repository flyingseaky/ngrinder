/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.agent.service;

<<<<<<< HEAD
import static net.grinder.message.console.AgentControllerState.INACTIVE;
import static net.grinder.message.console.AgentControllerState.WRONG_REGION;
import static org.ngrinder.agent.model.ClustedAgentRequest.RequestType.SHARE_AGENT_SYSTEM_DATA_MODEL;
import static org.ngrinder.agent.model.ClustedAgentRequest.RequestType.STOP_AGENT;
import static org.ngrinder.agent.repository.AgentManagerSpecification.startWithRegion;
import static org.ngrinder.agent.repository.AgentManagerSpecification.visible;
import static org.ngrinder.common.util.CollectionUtils.newHashMap;
import static org.ngrinder.common.util.TypeConvertUtil.cast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import net.grinder.common.processidentity.AgentIdentity;
import net.grinder.engine.controller.AgentControllerIdentityImplementation;
import net.grinder.util.thread.InterruptibleRunnable;
import net.sf.ehcache.Ehcache;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.ngrinder.agent.model.ClustedAgentRequest;
import org.ngrinder.agent.repository.AgentManagerRepository;
import org.ngrinder.infra.logger.CoreLogger;
import org.ngrinder.infra.schedule.ScheduledTask;
import org.ngrinder.model.AgentInfo;
import org.ngrinder.model.User;
import org.ngrinder.monitor.controller.model.SystemDataModel;
import org.ngrinder.perftest.service.AgentManager;
=======
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import net.grinder.common.processidentity.AgentIdentity;
import net.grinder.engine.controller.AgentControllerIdentityImplementation;
import net.sf.ehcache.Ehcache;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.ngrinder.agent.model.ClusteredAgentRequest;
import org.ngrinder.infra.logger.CoreLogger;
import org.ngrinder.infra.schedule.ScheduledTaskService;
import org.ngrinder.model.AgentInfo;
import org.ngrinder.model.User;
import org.ngrinder.monitor.controller.model.SystemDataModel;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.region.service.RegionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.Cache.ValueWrapper;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;

/**
 * Cluster enabled version of {@link AgentManagerService}.
 * 
=======
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static net.grinder.message.console.AgentControllerState.INACTIVE;
import static net.grinder.message.console.AgentControllerState.WRONG_REGION;
import static org.ngrinder.agent.model.ClusteredAgentRequest.RequestType.*;
import static org.ngrinder.agent.repository.AgentManagerSpecification.active;
import static org.ngrinder.agent.repository.AgentManagerSpecification.visible;
import static org.ngrinder.common.util.CollectionUtils.newArrayList;
import static org.ngrinder.common.util.CollectionUtils.newHashMap;
import static org.ngrinder.common.util.TypeConvertUtils.cast;

/**
 * Cluster enabled version of {@link AgentManagerService}.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.1
 */
public class ClusteredAgentManagerService extends AgentManagerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ClusteredAgentManagerService.class);

	@Autowired
<<<<<<< HEAD
	private CacheManager cacheManager;

	private Cache agentRequestCache;

	private Cache agentMonioringTargetsCache;

	@Autowired
	private ScheduledTask scheduledTask;
=======
	CacheManager cacheManager;

	private Cache agentRequestCache;

	private Cache agentMonitoringTargetsCache;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	@Autowired
	private RegionService regionService;

	/**
	 * Initialize.
	 */
	@PostConstruct
	public void init() {
<<<<<<< HEAD
		agentMonioringTargetsCache = cacheManager.getCache("agent_monitoring_targets");
		if (getConfig().isCluster()) {
			agentRequestCache = cacheManager.getCache("agent_request");
			scheduledTask.addScheduledTaskEvery3Sec(new InterruptibleRunnable() {
				@Override
				public void interruptibleRun() {
					List<String> keysWithExpiryCheck = cast(((Ehcache) agentRequestCache.getNativeCache())
							.getKeysWithExpiryCheck());
					String region = getConfig().getRegion() + "|";
					for (String each : keysWithExpiryCheck) {
						try {
							if (each.startsWith(region) && agentRequestCache.get(each) != null) {
								ClustedAgentRequest agentRequest = cast(agentRequestCache.get(each).get());
								AgentControllerIdentityImplementation agentIdentity = getLocalAgentIdentityByIpAndName(
										agentRequest.getAgentIp(), agentRequest.getAgentName());
								if (agentIdentity != null) {
									agentRequest.getRequestType().process(ClusteredAgentManagerService.this,
											agentIdentity);
								}
							}

						} catch (Exception e) {
							CoreLogger.LOGGER.error(e.getMessage(), e);
						}
						agentRequestCache.evict(each);
					}
				}
			});
		}
	}

	/**
	 * Run a scheduled task to check the agent statuses.
	 * 
	 * @since 3.1
	 */
	public void checkAgentStatus() {
		List<AgentInfo> changeAgents = new ArrayList<AgentInfo>();
		String curRegion = getConfig().getRegion();
=======
		super.init();
		agentMonitoringTargetsCache = cacheManager.getCache("agent_monitoring_targets");
		if (getConfig().isClustered()) {
			agentRequestCache = cacheManager.getCache("agent_request");
			scheduledTaskService.addFixedDelayedScheduledTask(new Runnable() {
				@Override
				public void run() {
					List<String> keys = cast(((Ehcache) agentRequestCache.getNativeCache())
							.getKeysWithExpiryCheck());
					String region = getConfig().getRegion() + "|";
					for (String each : keys) {
						if (each.startsWith(region)) {
							if (agentRequestCache.get(each) != null) {
								try {
									ClusteredAgentRequest agentRequest = cast(agentRequestCache.get(each).get());
									if (agentRequest.getRequestType() ==
											ClusteredAgentRequest.RequestType.EXPIRE_LOCAL_CACHE) {
										expireLocalCache();
									} else {
										AgentControllerIdentityImplementation agentIdentity = getAgentIdentityByIpAndName(
												agentRequest.getAgentIp(), agentRequest.getAgentName());
										if (agentIdentity != null) {
											agentRequest.getRequestType().process(ClusteredAgentManagerService.this,
													agentIdentity);
										}
									}
									agentRequestCache.evict(each);
								} catch (Exception e) {
									CoreLogger.LOGGER.error(e.getMessage(), e);
								}
							}
						}
					}
				}
			}, 3000);
		}
	}

	@Override
	public void checkAgentStatePeriodically() {
		super.checkAgentStatePeriodically();
		collectAgentSystemData();
	}

	/**
	 * Run a scheduled task to check the agent statuses.
	 *
	 * @since 3.1
	 */
	@Override
	public void checkAgentState() {
		List<AgentInfo> newAgents = newArrayList(0);
		List<AgentInfo> updatedAgents = newArrayList(0);
		List<AgentInfo> stateUpdatedAgents = newArrayList(0);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		Set<AgentIdentity> allAttachedAgents = getAgentManager().getAllAttachedAgents();
		Map<String, AgentControllerIdentityImplementation> attachedAgentMap = newHashMap(allAttachedAgents);
		for (AgentIdentity agentIdentity : allAttachedAgents) {
			AgentControllerIdentityImplementation existingAgent = cast(agentIdentity);
<<<<<<< HEAD
			attachedAgentMap.put(createAgentKey(existingAgent), existingAgent);
		}

		List<AgentInfo> agentsInDB = getAgentRepository().findAll(startWithRegion(curRegion));
		Map<String, AgentInfo> agentsInDBMap = Maps.newHashMap();
		// step1. check all agents in DB, whether they are attached to
		// controller.
		for (AgentInfo eachAgentInDB : agentsInDB) {
			String keyOfAgentInDB = createAgentKey(eachAgentInDB);
			agentsInDBMap.put(keyOfAgentInDB, eachAgentInDB);
			AgentControllerIdentityImplementation agentIdentity = attachedAgentMap.remove(keyOfAgentInDB);

			if (agentIdentity != null) {
				// if the agent attached to current controller
				if (!hasSamePortAndStatus(eachAgentInDB, agentIdentity)) {
					fillUp(eachAgentInDB, agentIdentity);
					changeAgents.add(eachAgentInDB);
				} else if (!StringUtils.equals(eachAgentInDB.getRegion(), agentIdentity.getRegion())) {
					fillUp(eachAgentInDB, agentIdentity);
					eachAgentInDB.setStatus(WRONG_REGION);
					eachAgentInDB.setApproved(false);
					changeAgents.add(eachAgentInDB);
				}

			} else { // the agent in DB is not attached to current controller

				if (eachAgentInDB.getStatus() != INACTIVE) {
					eachAgentInDB.setStatus(INACTIVE);
					changeAgents.add(eachAgentInDB);
=======
			attachedAgentMap.put(createKey(existingAgent), existingAgent);
		}
		Map<String, AgentInfo> agentsInDBMap = Maps.newHashMap();
		// step1. check all agents in DB, whether they are attached to
		// controller.
		for (AgentInfo eachAgentInDB : getAllLocal()) {
			String keyOfAgentInDB = createKey(eachAgentInDB);
			agentsInDBMap.put(keyOfAgentInDB, eachAgentInDB);
			AgentControllerIdentityImplementation agentIdentity = attachedAgentMap.remove(keyOfAgentInDB);
			if (agentIdentity != null) {
				// if the agent attached to current controller
				if (!isCurrentRegion(agentIdentity)) {
					if (eachAgentInDB.getState() != WRONG_REGION) {
						eachAgentInDB.setApproved(false);
						eachAgentInDB.setRegion(getConfig().getRegion());
						eachAgentInDB.setState(WRONG_REGION);
						updatedAgents.add(eachAgentInDB);
					}
				} else if (!hasSameInfo(eachAgentInDB, agentIdentity)) {
					fillUp(eachAgentInDB, agentIdentity);
					updatedAgents.add(eachAgentInDB);
				} else if (!hasSameState(eachAgentInDB, agentIdentity)) {
					eachAgentInDB.setState(getAgentManager().getAgentState(agentIdentity));
					stateUpdatedAgents.add(eachAgentInDB);
				} else if (eachAgentInDB.getApproved() == null) {
					updatedAgents.add(fillUpApproval(eachAgentInDB));
				}
			} else { // the agent in DB is not attached to current controller
				if (eachAgentInDB.getState() != INACTIVE) {
					eachAgentInDB.setState(INACTIVE);
					stateUpdatedAgents.add(eachAgentInDB);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				}
			}
		}

		// step2. check all attached agents, whether they are new, and not saved
		// in DB.
		for (AgentControllerIdentityImplementation agentIdentity : attachedAgentMap.values()) {
<<<<<<< HEAD
			AgentInfo agentInfo = getAgentRepository().findByIpAndHostName(agentIdentity.getIp(),
					agentIdentity.getName());
			if (agentInfo == null) {
				agentInfo = new AgentInfo();
			}
			if (StringUtils.equals(extractRegionFromAgentRegion(agentIdentity.getRegion()), curRegion)) {
				AgentInfo newAgentInfo = fillUp(agentInfo, agentIdentity);
				changeAgents.add(newAgentInfo);
			} else {
				if (agentInfo.getStatus() != WRONG_REGION) {
					AgentInfo newAgentInfo = fillUp(agentInfo, agentIdentity);
					agentInfo.setStatus(WRONG_REGION);
					agentInfo.setApproved(false);
					changeAgents.add(newAgentInfo);
				}
			}
		}

		// step3. update into DB
		getAgentRepository().save(changeAgents);
	}

	private boolean hasSamePortAndStatus(AgentInfo agentInfo, AgentControllerIdentityImplementation agentIdentity) {
		if (agentInfo == null) {
			return false;
		}
		AgentManager agentManager = getAgentManager();
		return agentInfo.getPort() == agentManager.getAgentConnectingPort(agentIdentity)
				&& agentInfo.getStatus() == agentManager.getAgentState(agentIdentity);
	}

=======
			AgentInfo agentInfo = agentManagerRepository.findByIpAndHostName(
					agentIdentity.getIp(),
					agentIdentity.getName());
			if (agentInfo == null) {
				agentInfo = new AgentInfo();
				newAgents.add(fillUp(agentInfo, agentIdentity));
			} else {
				updatedAgents.add(fillUp(agentInfo, agentIdentity));
			}
			if (!isCurrentRegion(agentIdentity)) {
				agentInfo.setState(WRONG_REGION);
				agentInfo.setApproved(false);
			}
		}

		cachedLocalAgentService.updateAgents(newAgents, updatedAgents, stateUpdatedAgents, null);
		if (!newAgents.isEmpty() || !updatedAgents.isEmpty()) {
			expireLocalCache();
		}
	}


>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private Gson gson = new Gson();

	/**
	 * Collect the agent system info every second.
<<<<<<< HEAD
	 * 
	 */
	@Scheduled(fixedDelay = 1000)
	public void collectAgentSystemData() {
		Ehcache nativeCache = (Ehcache) agentMonioringTargetsCache.getNativeCache();
		List<String> keysWithExpiryCheck = cast(nativeCache.getKeysWithExpiryCheck());
		AgentManagerRepository agentManagerRepository = getAgentManagerRepository();
		if (keysWithExpiryCheck.isEmpty()) {
			return;
		}
		List<AgentInfo> agentInfos = new ArrayList<AgentInfo>();
		for (String each : keysWithExpiryCheck) {
			ValueWrapper value = agentMonioringTargetsCache.get(each);
			AgentControllerIdentityImplementation agentIdentity = cast(value.get());
			if (value != null && agentIdentity != null) {
				AgentInfo found = agentManagerRepository.findByIpAndHostName(agentIdentity.getIp(),
						agentIdentity.getName());
				found.setSystemStat(gson.toJson(getSystemDataModel(agentIdentity)));
				agentInfos.add(found);
			}
		}
		agentManagerRepository.save(agentInfos);
=======
	 */
	public void collectAgentSystemData() {
		Ehcache nativeCache = (Ehcache) agentMonitoringTargetsCache.getNativeCache();
		List<String> keysWithExpiryCheck = cast(nativeCache.getKeysWithExpiryCheck());
		for (String each : keysWithExpiryCheck) {
			ValueWrapper value = agentMonitoringTargetsCache.get(each);
			AgentControllerIdentityImplementation agentIdentity = cast(value.get());
			if (agentIdentity != null) {
				// Is Same Region
				if (isCurrentRegion(agentIdentity)) {
					try {
						updateSystemStat(agentIdentity);
					} catch (IllegalStateException e) {
						LOGGER.error("error while update system stat.");
					}
				}
			}
		}
	}

	public void updateSystemStat(final AgentControllerIdentityImplementation agentIdentity) {
		cachedLocalAgentService.doSthInTransaction(new Runnable() {
			public void run() {
				agentManagerRepository.updateSystemStat(agentIdentity.getIp(),
						agentIdentity.getName(),
						gson.toJson(getSystemDataModel(agentIdentity)));
			}
		});
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	private SystemDataModel getSystemDataModel(AgentIdentity agentIdentity) {
		return getAgentManager().getSystemDataModel(agentIdentity);
	}

<<<<<<< HEAD
	/**
	 * Get all visible agents from DB.
	 * 
	 * @return agent list
	 */
	@Override
	public List<AgentInfo> getAllVisibleAgentInfoFromDB() {
		List<AgentInfo> result = Lists.newArrayList();
		Set<String> regions = getRegions();
		for (AgentInfo each : getAgentRepository().findAll(visible())) {
			if (regions.contains(extractRegionFromAgentRegion(each.getRegion()))) {
				result.add(each);
			}
		}
		return result;
	}

	/**
	 * Get the available agent count map in all regions of the user, including
	 * the free agents and user specified agents.
	 * 
	 * @param user
	 *            current user
	 * @return user available agent count map
	 */
	@Override
	@Transactional
	public Map<String, MutableInt> getUserAvailableAgentCountMap(User user) {
=======
	public List<AgentInfo> getAllActive() {
		return filterOnlyActiveRegion(agentManagerRepository.findAll(active()));
	}

	public List<AgentInfo> getAllVisible() {
		return filterOnlyActiveRegion(agentManagerRepository.findAll(visible()));
	}

	private List<AgentInfo> filterOnlyActiveRegion(List<AgentInfo> agents) {
		final Set<String> regions = getRegions();
		return Lists.newArrayList(Iterables.filter(agents,
				new Predicate<AgentInfo>() {
					@Override
					public boolean apply(@Nullable AgentInfo input) {
						return input != null && regions.contains(extractRegionFromAgentRegion(input.getRegion()));
					}
				}));
	}


	/**
	 * Get the available agent count map in all regions of the user, including
	 * the free agents and user specified agents.
	 *
	 * @param user current user
	 * @return user available agent count map
	 */
	@Override
	public Map<String, MutableInt> getAvailableAgentCountMap(User user) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		Set<String> regions = getRegions();
		Map<String, MutableInt> availShareAgents = newHashMap(regions);
		Map<String, MutableInt> availUserOwnAgent = newHashMap(regions);
		for (String region : regions) {
			availShareAgents.put(region, new MutableInt(0));
			availUserOwnAgent.put(region, new MutableInt(0));
		}
<<<<<<< HEAD
		String myAgentSuffix = "_owned_" + user.getUserId();

		for (AgentInfo agentInfo : getAllActiveAgentInfoFromDB()) {
=======
		String myAgentSuffix = "owned_" + user.getUserId();

		for (AgentInfo agentInfo : getAllActive()) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			// Skip all agents which are disapproved, inactive or
			// have no region prefix.
			if (!agentInfo.isApproved()) {
				continue;
			}

			String fullRegion = agentInfo.getRegion();
			String region = extractRegionFromAgentRegion(fullRegion);
			if (StringUtils.isBlank(region) || !regions.contains(region)) {
				continue;
			}
			// It's my own agent
			if (fullRegion.endsWith(myAgentSuffix)) {
				incrementAgentCount(availUserOwnAgent, region, user.getUserId());
<<<<<<< HEAD
			} else if (fullRegion.contains("_owned_")) {
				// If it's the others agent.. skip..
				continue;
			} else {
=======
			} else if (!fullRegion.contains("owned_")) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				incrementAgentCount(availShareAgents, region, user.getUserId());
			}
		}

		int maxAgentSizePerConsole = getMaxAgentSizePerConsole();

		for (String region : regions) {
			MutableInt mutableInt = availShareAgents.get(region);
			int shareAgentCount = mutableInt.intValue();
			mutableInt.setValue(Math.min(shareAgentCount, maxAgentSizePerConsole));
			mutableInt.add(availUserOwnAgent.get(region));
		}
		return availShareAgents;
	}

	protected Set<String> getRegions() {
<<<<<<< HEAD
		return regionService.getRegions().keySet();
	}

	String extractRegionFromAgentRegion(String agentRegion) {
		if (agentRegion.contains("_owned_")) {
			return agentRegion.substring(0, agentRegion.indexOf("_owned_"));
		}
		return agentRegion;
	}

=======
		return regionService.getAll().keySet();
	}

	protected boolean isCurrentRegion(AgentControllerIdentityImplementation agentIdentity) {
		return StringUtils.equals(extractRegionFromAgentRegion(agentIdentity.getRegion()), getConfig().getRegion());
	}


>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private void incrementAgentCount(Map<String, MutableInt> agentMap, String region, String userId) {
		if (!agentMap.containsKey(region)) {
			LOGGER.warn("Region :{} not exist in cluster nor owned by user:{}.", region, userId);
		} else {
			agentMap.get(region).increment();
		}
	}

<<<<<<< HEAD
	/**
	 * Get all agents attached of this region from DB.
	 * 
	 * This method is cluster aware. If it's cluster mode it return all agents
	 * attached in this region.
	 * 
	 * @return agent list
	 */
	@Override
	public List<AgentInfo> getLocalAgentListFromDB() {
		return getAgentRepository().findAll(startWithRegion(getConfig().getRegion()));
=======
	@Override
	public AgentInfo approve(Long id, boolean approve) {
		AgentInfo agent = super.approve(id, approve);
		if (agent != null) {
			agentRequestCache.put(extractRegionFromAgentRegion(agent.getRegion()) + "|" + createKey(agent),
					new ClusteredAgentRequest(agent.getIp(), agent.getName(), EXPIRE_LOCAL_CACHE));
		}
		return agent;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Stop agent. In cluster mode, it queues the agent stop request to
	 * agentRequestCache.
<<<<<<< HEAD
	 * 
	 * @param id
	 *            agent id in db
	 * 
	 */
	@Override
	public void stopAgent(Long id) {
		AgentInfo agent = getAgent(id, false);
		if (agent == null) {
			return;
		}
		agentRequestCache.put(extractRegionFromAgentRegion(agent.getRegion()) + "|" + createAgentKey(agent),
				new ClustedAgentRequest(agent.getIp(), agent.getName(), STOP_AGENT));
=======
	 *
	 * @param id agent id in db
	 */
	@Override
	public void stopAgent(Long id) {
		AgentInfo agent = getOne(id);
		if (agent == null) {
			return;
		}
		agentRequestCache.put(extractRegionFromAgentRegion(agent.getRegion()) + "|" + createKey(agent),
				new ClusteredAgentRequest(agent.getIp(), agent.getName(), STOP_AGENT));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Add the agent system data model share request on cache.
<<<<<<< HEAD
	 * 
	 * @param id
	 *            agent id in db.
	 */
	@Override
	public void requestShareAgentSystemDataModel(Long id) {
		AgentInfo agent = getAgent(id, false);
		if (agent == null) {
			return;
		}
		agentRequestCache.put(extractRegionFromAgentRegion(agent.getRegion()) + "|" + createAgentKey(agent),
				new ClustedAgentRequest(agent.getIp(), agent.getName(), SHARE_AGENT_SYSTEM_DATA_MODEL));
=======
	 *
	 * @param id agent id in db.
	 */
	@Override
	public void requestShareAgentSystemDataModel(Long id) {
		AgentInfo agent = getOne(id);
		if (agent == null) {
			return;
		}
		agentRequestCache.put(extractRegionFromAgentRegion(agent.getRegion()) + "|" + createKey(agent),
				new ClusteredAgentRequest(agent.getIp(), agent.getName(), SHARE_AGENT_SYSTEM_DATA_MODEL));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the agent system data model for the given IP. This method is cluster
	 * aware.
<<<<<<< HEAD
	 * 
	 * @param ip
	 *            agent ip
	 * @param name
	 *            agent name
	 * 
	 * @return {@link SystemDataModel} instance.
	 */
	@Override
	public SystemDataModel getAgentSystemDataModel(String ip, String name) {
		AgentInfo found = getAgentRepository().findByIpAndHostName(ip, name);
=======
	 *
	 * @param ip   agent ip
	 * @param name agent name
	 * @return {@link SystemDataModel} instance.
	 */
	@Override
	public SystemDataModel getSystemDataModel(String ip, String name) {
		AgentInfo found = agentManagerRepository.findByIpAndHostName(ip, name);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		String systemStat = (found == null) ? null : found.getSystemStat();
		return (StringUtils.isEmpty(systemStat)) ? new SystemDataModel() : gson.fromJson(systemStat,
				SystemDataModel.class);
	}

	/**
	 * Register agent monitoring target. This method should be called in the
	 * controller in which the given agent exists.
<<<<<<< HEAD
	 * 
	 * @param agentIdentity
	 *            agent identity
	 */
	public void addAgentMonitoringTarget(AgentControllerIdentityImplementation agentIdentity) {
		agentMonioringTargetsCache.put(createAgentKey(agentIdentity), agentIdentity);
=======
	 *
	 * @param agentIdentity agent identity
	 */
	public void addAgentMonitoringTarget(AgentControllerIdentityImplementation agentIdentity) {
		agentMonitoringTargetsCache.put(createKey(agentIdentity), agentIdentity);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Stop agent.
<<<<<<< HEAD
	 * 
	 * @param agentIdentity
	 *            agent identity to be stopped.
=======
	 *
	 * @param agentIdentity agent identity to be stopped.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void stopAgent(AgentControllerIdentityImplementation agentIdentity) {
		getAgentManager().stopAgent(agentIdentity);
	}
<<<<<<< HEAD
=======


	/**
	 * Update agent by id.
	 *
	 * @param id agent id
	 */
	@Override
	public void update(Long id) {
		AgentInfo agent = getOne(id);
		if (agent == null) {
			return;
		}
		agentRequestCache.put(extractRegionFromAgentRegion(agent.getRegion()) + "|" + createKey(agent),
				new ClusteredAgentRequest(agent.getIp(), agent.getName(), UPDATE_AGENT));
	}

	/**
	 * Clean up the agents from db which belongs to the inactive regions.
	 */
	@Transactional
	public void cleanup() {
		super.cleanup();
		final Set<String> regions = getRegions();
		for (AgentInfo each : agentManagerRepository.findAll()) {
			if (!regions.contains(extractRegionFromAgentRegion(each.getRegion()))) {
				agentManagerRepository.delete(each);
			}
		}
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
