/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.perftest.service.monitor;

<<<<<<< HEAD
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ngrinder.monitor.share.domain.SystemInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

=======
import org.apache.commons.io.IOUtils;
import org.ngrinder.infra.schedule.ScheduledTaskService;
import org.ngrinder.monitor.share.domain.SystemInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
/**
 * Used to save JMX connect for every request that want to observe monitor real-time system information.
 */
@Service
<<<<<<< HEAD
@Scope(value = "singleton")
public class MonitorInfoStore {

	@Autowired
	private ApplicationContext applicationContext;

	private Map<String, MonitorClientSerivce> monitorInfoMap = Collections
			.synchronizedMap(new HashMap<String, MonitorClientSerivce>());

	/**
	 * Get monitor data from mbean client.
	 * 
	 * @param ip
	 *            ip
	 * @param port
	 *            port
	 * @return {@link SystemInfo}
	 */
	public SystemInfo getSystemInfo(String ip, int port) {
		MonitorClientSerivce monitorClient = monitorInfoMap.get(ip);
		if (monitorClient == null) {
			monitorClient = applicationContext.getBean(MonitorClientSerivce.class);
			monitorClient.init(ip, port);
			monitorClient.setLastAccessedTime(System.currentTimeMillis());
			add(ip, monitorClient);
		}
		monitorClient.setLastAccessedTime(System.currentTimeMillis());
		return monitorClient.getMonitorData();
	}

	private void add(String ip, MonitorClientSerivce monitorClient) {
		synchronized (this) {
			monitorInfoMap.put(ip, monitorClient);
		}
=======
public class MonitorInfoStore implements Runnable {
	private static final Logger LOGGER = LoggerFactory.getLogger(MonitorInfoStore.class);

	@Autowired
	private ScheduledTaskService scheduledTaskService;
	private Map<String, MonitorClientService> monitorClientMap = new ConcurrentHashMap<String, MonitorClientService>();


	@PostConstruct
	public void init() {
		scheduledTaskService.addFixedDelayedScheduledTask(this, 30000);
	}

	/**
	 * Get monitor data from mbean client.
	 *
	 * @param ip   ip
	 * @param port port
	 * @return {@link org.ngrinder.monitor.share.domain.SystemInfo}
	 */
	public SystemInfo getSystemInfo(String ip, int port) {
		MonitorClientService monitorClient = monitorClientMap.get(ip);
		if (monitorClient == null) {
			monitorClient = new MonitorClientService(ip, port);
			monitorClient.init();
			IOUtils.closeQuietly(monitorClientMap.put(ip, monitorClient));
		}
		monitorClient.update();
		monitorClient.setLastAccessedTime(System.currentTimeMillis());
		return monitorClient.getSystemInfo();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Delete the unused monitor clients periodically.
	 */
<<<<<<< HEAD
	@Scheduled(fixedDelay = 30000)
	public void deleteUnusedMonitorClient() {
		for (Entry<String, MonitorClientSerivce> each : monitorInfoMap.entrySet()) {
			if ((System.currentTimeMillis() - each.getValue().getLastAccessedTime()) > 30000) {
				remove(each.getKey());
=======
	public void run() {
		for (Entry<String, MonitorClientService> each : monitorClientMap.entrySet()) {
			if ((System.currentTimeMillis() - each.getValue().getLastAccessedTime()) > 30000) {
				close(each.getKey());
				LOGGER.info("Remove unused monitor {}", each.getKey());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			}
		}
	}

	/**
	 * Close mbean client connected to the given ip.
<<<<<<< HEAD
	 * 
	 * @param ip
	 *            ip
	 */
	public void remove(String ip) {
		synchronized (this) {
			MonitorClientSerivce monitorClient = monitorInfoMap.get(ip);
			if (monitorClient == null) {
				return;
			}
			monitorClient.closeMbeanClient();
			monitorInfoMap.remove(ip);
		}
	}

=======
	 *
	 * @param ip ip
	 */
	public void close(String ip) {
		IOUtils.closeQuietly(monitorClientMap.remove(ip));
	}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
