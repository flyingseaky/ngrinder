/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.perftest.service;

<<<<<<< HEAD
import static org.apache.commons.lang.ObjectUtils.defaultIfNull;
import static org.ngrinder.model.Status.CANCELED;
import static org.ngrinder.model.Status.DISTRIBUTE_FILES;
import static org.ngrinder.model.Status.DISTRIBUTE_FILES_FINISHED;
import static org.ngrinder.model.Status.START_AGENTS;
import static org.ngrinder.model.Status.START_AGENTS_FINISHED;
import static org.ngrinder.model.Status.START_CONSOLE;
import static org.ngrinder.model.Status.START_TESTING;
import static org.ngrinder.model.Status.TESTING;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import net.grinder.SingleConsole;
import net.grinder.SingleConsole.ConsoleShutdownListener;
import net.grinder.StopReason;
import net.grinder.common.GrinderProperties;
import net.grinder.console.model.ConsoleProperties;
import net.grinder.util.ListenerHelper;
import net.grinder.util.ListenerSupport;
<<<<<<< HEAD

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.ngrinder.common.constant.NGrinderConstants;
import org.ngrinder.extension.OnTestLifeCycleRunnable;
import org.ngrinder.extension.OnTestSamplingRunnable;
import org.ngrinder.infra.annotation.RuntimeOnlyComponent;
import org.ngrinder.infra.config.Config;
import org.ngrinder.infra.plugin.PluginManager;
import org.ngrinder.model.AgentInfo;
import org.ngrinder.model.PerfTest;
import org.ngrinder.model.Status;
import org.ngrinder.perftest.model.NullSingleConsole;
import org.ngrinder.perftest.service.samplinglistener.AgentDieHardListener;
import org.ngrinder.perftest.service.samplinglistener.AgentLostDetectionListener;
import org.ngrinder.perftest.service.samplinglistener.MonitorCollectorListener;
import org.ngrinder.perftest.service.samplinglistener.PerfTestSamplingCollectorListener;
import org.ngrinder.perftest.service.samplinglistener.PluginRunListener;
import org.ngrinder.script.handler.ScriptHandler;
import org.python.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;

import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;

/**
 * {@link PerfTest} run scheduler.
 * 
 * This class is responsible to execute/finish the performance test. The job is
 * started from {@link #startTest()} and {@link #finishTest()} method. These
 * methods are scheduled by Spring Task.
 * 
 * @author JunHo Yoon
 * @since 3.0
 */
@RuntimeOnlyComponent
public class PerfTestRunnable implements NGrinderConstants {

	private static final Logger LOG = LoggerFactory.getLogger(PerfTestRunnable.class);

=======
import net.grinder.util.UnitUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.ngrinder.common.constant.ControllerConstants;
import org.ngrinder.extension.OnTestLifeCycleRunnable;
import org.ngrinder.extension.OnTestSamplingRunnable;
import org.ngrinder.infra.config.Config;
import org.ngrinder.infra.plugin.PluginManager;
import org.ngrinder.infra.schedule.ScheduledTaskService;
import org.ngrinder.model.PerfTest;
import org.ngrinder.model.Status;
import org.ngrinder.perftest.model.NullSingleConsole;
import org.ngrinder.perftest.service.samplinglistener.*;
import org.ngrinder.script.handler.ScriptHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang.ObjectUtils.defaultIfNull;
import static org.ngrinder.common.constant.ClusterConstants.PROP_CLUSTER_SAFE_DIST;
import static org.ngrinder.common.util.AccessUtils.getSafe;
import static org.ngrinder.model.Status.*;

/**
 * {@link PerfTest} run scheduler.
 * <p/>
 * This class is responsible to execute/finish the performance test. The job is
 * started from {@link #doStart()}  and {@link #doFinish()} method. These
 * methods are scheduled by Spring Task.
 *
 * @author JunHo Yoon
 * @since 3.0
 */
@Profile("production")
@Component
public class PerfTestRunnable implements ControllerConstants {

	private static final Logger LOG = LoggerFactory.getLogger(PerfTestRunnable.class);

	@SuppressWarnings("SpringJavaAutowiringInspection")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Autowired
	private PerfTestService perfTestService;

	@Autowired
	private ConsoleManager consoleManager;

	@Autowired
	private AgentManager agentManager;

	@Autowired
	private PluginManager pluginManager;

	@Autowired
	private Config config;

<<<<<<< HEAD
	private List<OnTestSamplingRunnable> testSamplingRunnables;

	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * Initialize plugin manager to register plugin update event.
	 */
	@PostConstruct
	public void init() {
		pluginManager.addPluginUpdateEvent(this);
		pluginInit();
	}

	private void pluginInit() {
		this.testSamplingRunnables = pluginManager.getEnabledModulesByClass(OnTestSamplingRunnable.class);
	}

	/**
	 * Event handler for plugin enable.
	 * 
	 * @param event
	 *            event
	 */
	@PluginEventListener
	public void onPluginEnabled(PluginEnabledEvent event) {
		pluginInit();
	}

	/**
	 * Event handler for plugin disable.
	 * 
	 * @param event
	 *            event
	 */
	@PluginEventListener
	public void onPluginDisabled(PluginDisabledEvent event) {
		pluginInit();
=======
	@Autowired
	private ScheduledTaskService scheduledTaskService;

	private Runnable startRunnable;

	private Runnable finishRunnable;

	@PostConstruct
	public void init() {
		// Clean up db first.
		doFinish(true);

		this.startRunnable = new Runnable() {
			@Override
			public void run() {
				startPeriodically();
			}
		};
		scheduledTaskService.addFixedDelayedScheduledTask(startRunnable, PERFTEST_RUN_FREQUENCY_MILLISECONDS);
		this.finishRunnable = new Runnable() {
			@Override
			public void run() {
				finishPeriodically();
			}
		};
		scheduledTaskService.addFixedDelayedScheduledTask(finishRunnable, PERFTEST_RUN_FREQUENCY_MILLISECONDS);

	}

	@PreDestroy
	public void destroy() {
		scheduledTaskService.removeScheduledJob(this.startRunnable);
		scheduledTaskService.removeScheduledJob(this.finishRunnable);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Scheduled method for test execution. This method dispatches the test
	 * candidates and run one of them. This method is responsible until a test
	 * is executed.
	 */
<<<<<<< HEAD
	@Scheduled(fixedDelay = PERFTEST_RUN_FREQUENCY_MILLISECONDS)
	public void startTest() {
=======
	public void startPeriodically() {
		doStart();
	}

	void doStart() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		if (config.hasNoMoreTestLock()) {
			return;
		}
		// Block if the count of testing exceed the limit
<<<<<<< HEAD
		if (!perfTestService.canExecuteTestMore()) {
=======
		if (canExecuteMore()) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			// LOG MORE
			List<PerfTest> currentlyRunningTests = perfTestService.getCurrentlyRunningTest();
			LOG.debug("Currently running test is {}. No more tests can not run.", currentlyRunningTests.size());
			return;
		}
		// Find out next ready perftest
<<<<<<< HEAD
		PerfTest runCandidate = perfTestService.getPerfTestCandiate();
=======
		PerfTest runCandidate = getRunnablePerfTest();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		if (runCandidate == null) {
			return;
		}

		if (!isScheduledNow(runCandidate)) {
			// this test project is reserved,but it isn't yet going to run test
			// right now.
			return;
		}

<<<<<<< HEAD
		if (exceedMoreAgent(runCandidate)) {
			return;
		}
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		if (!hasEnoughFreeAgents(runCandidate)) {
			return;
		}

		doTest(runCandidate);
	}

<<<<<<< HEAD
=======
	private PerfTest getRunnablePerfTest() {
		return perfTestService.getNextRunnablePerfTestPerfTestCandidate();
	}

	private boolean canExecuteMore() {
		return consoleManager.getConsoleInUse().size() >= perfTestService.getMaximumConcurrentTestCount();
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private boolean isScheduledNow(PerfTest test) {
		Date current = new Date();
		Date scheduledDate = DateUtils
				.truncate((Date) defaultIfNull(test.getScheduledTime(), current), Calendar.MINUTE);
		return current.after(scheduledDate);
	}

<<<<<<< HEAD
	/**
	 * Check the approved agent availability for the given {@link PerfTest}.
	 * 
	 * @param test
	 *            {@link PerfTest}
	 * @return true if enough agents
	 */
	protected boolean exceedMoreAgent(PerfTest test) {
		int size = agentManager.getAllApprovedAgents(test.getCreatedUser()).size();
		if (test.getAgentCount() > size) {
			perfTestService.markAbromalTermination(test,
					"The test is tried to execute but this test requires more agents "
							+ "than the count of approved agents." + "\n- Current all agent size : " + size
							+ "  / Requested : " + test.getAgentCount() + "\n");
			return true;
		}
		return false;
	}

	/**
	 * Check the free agent availability for the given {@link PerfTest}.
	 * 
	 * @param test
	 *            {@link PerfTest}
=======

	/**
	 * Check the free agent availability for the given {@link PerfTest}.
	 *
	 * @param test {@link PerfTest}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return true if enough agents
	 */
	protected boolean hasEnoughFreeAgents(PerfTest test) {
		int size = agentManager.getAllFreeApprovedAgentsForUser(test.getCreatedUser()).size();
<<<<<<< HEAD
		if (test.getAgentCount() > size) {
			perfTestService.markProgress(test, "The test is tried to execute but there is not enough free agents."
					+ "\n- Current free agent size : " + size + "  / Requested : " + test.getAgentCount() + "\n");
=======
		if (test.getAgentCount() != null && test.getAgentCount() > size) {
			perfTestService.markProgress(test, "The test is tried to execute but there is not enough free agents."
					+ "\n- Current free agent count : " + size + "  / Requested : " + test.getAgentCount() + "\n");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			return false;
		}
		return true;
	}

	/**
	 * Run the given test.
<<<<<<< HEAD
	 * 
	 * If fails, it marks STOP_BY_ERROR in the given {@link PerfTest} status
	 * 
	 * @param perfTest
	 *            perftest instance;
=======
	 * <p/>
	 * If fails, it marks STOP_BY_ERROR in the given {@link PerfTest} status
	 *
	 * @param perfTest perftest instance;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void doTest(final PerfTest perfTest) {
		SingleConsole singleConsole = null;
		try {
			singleConsole = startConsole(perfTest);
			ScriptHandler prepareDistribution = perfTestService.prepareDistribution(perfTest);
			GrinderProperties grinderProperties = perfTestService.getGrinderProperties(perfTest, prepareDistribution);
			startAgentsOn(perfTest, grinderProperties, checkCancellation(singleConsole));
<<<<<<< HEAD
			distributeFileOn(perfTest, grinderProperties, checkCancellation(singleConsole));
			singleConsole.setReportPath(perfTestService.getReportFileDirectory(perfTest));
			runTestOn(perfTest, grinderProperties, checkCancellation(singleConsole));
		} catch (SinlgeConsolCancellationException ex) {
			// In case of error, mark the occurs error on perftest.
			doCancel(perfTest, singleConsole);
			notifyFinsish(perfTest, StopReason.CANCEL_BY_USER);
		} catch (Exception e) {
			// In case of error, mark the occurs error on perftest.
			LOG.error("Error while excuting test: {} - {} ", perfTest.getTestIdentifier(), e.getMessage());
			LOG.debug("Stack Trace is : ", e);
			doTerminate(perfTest, singleConsole);
			notifyFinsish(perfTest, StopReason.ERROR_WHILE_PREPARE);
=======
			distributeFileOn(perfTest, checkCancellation(singleConsole));

			singleConsole.setReportPath(perfTestService.getReportFileDirectory(perfTest));
			runTestOn(perfTest, grinderProperties, checkCancellation(singleConsole));
		} catch (SingleConsoleCancellationException ex) {
			// In case of error, mark the occurs error on perftest.
			doCancel(perfTest, singleConsole);
			notifyFinish(perfTest, StopReason.CANCEL_BY_USER);
		} catch (Exception e) {
			// In case of error, mark the occurs error on perftest.
			LOG.error("Error while executing test: {} - {} ", perfTest.getTestIdentifier(), e.getMessage());
			LOG.debug("Stack Trace is : ", e);
			doTerminate(perfTest, singleConsole);
			notifyFinish(perfTest, StopReason.ERROR_WHILE_PREPARE);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
	 * Check the cancellation status on console.
<<<<<<< HEAD
	 * 
	 * @param singleConsole
	 *            console
=======
	 *
	 * @param singleConsole console
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return true if cancellation is requested.
	 */
	SingleConsole checkCancellation(SingleConsole singleConsole) {
		if (singleConsole.isCanceled()) {
<<<<<<< HEAD
			throw new SinlgeConsolCancellationException("Single Console " + singleConsole.getConsolePort()
=======
			throw new SingleConsoleCancellationException("Single Console " + singleConsole.getConsolePort()
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					+ " is canceled");
		}
		return singleConsole;
	}

	/**
	 * Start a console for given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
=======
	 *
	 * @param perfTest perftest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return started console
	 */
	SingleConsole startConsole(PerfTest perfTest) {
		perfTestService.markStatusAndProgress(perfTest, START_CONSOLE, "Console is being prepared.");
		// get available consoles.
		ConsoleProperties consoleProperty = perfTestService.createConsoleProperties(perfTest);
<<<<<<< HEAD
		SingleConsole singleConsole = consoleManager.getAvailableConsole(perfTest.getTestIdentifier(), consoleProperty);
=======
		SingleConsole singleConsole = consoleManager.getAvailableConsole(consoleProperty);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		singleConsole.start();
		perfTestService.markPerfTestConsoleStart(perfTest, singleConsole.getConsolePort());
		return singleConsole;
	}

	/**
	 * Distribute files to agents.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
	 * @param grinderProperties
	 *            grinder properties
	 * @param singleConsole
	 *            console to be used.
	 */
	void distributeFileOn(final PerfTest perfTest, GrinderProperties grinderProperties, SingleConsole singleConsole) {
		// Distribute files
		perfTestService.markStatusAndProgress(perfTest, DISTRIBUTE_FILES, "All necessary files are being distributed.");
		ListenerSupport<SingleConsole.FileDistributionListener> listener = ListenerHelper.create();
		final int safeThreadHold = getSafeTransitionThreadHold();
=======
	 *
	 * @param perfTest      perftest
	 * @param singleConsole console to be used.
	 */
	void distributeFileOn(final PerfTest perfTest, SingleConsole singleConsole) {
		// Distribute files
		perfTestService.markStatusAndProgress(perfTest, DISTRIBUTE_FILES, "All necessary files are being distributed.");
		ListenerSupport<SingleConsole.FileDistributionListener> listener = ListenerHelper.create();
		final int safeThreadHold = getSafeTransmissionThreshold();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		listener.add(new SingleConsole.FileDistributionListener() {
			@Override
			public void distributed(String fileName) {
				perfTestService.markProgress(perfTest, " - " + fileName);
			}

			@Override
			public boolean start(File dir, boolean safe) {
				if (safe) {
					perfTestService.markProgress(perfTest, "Safe file distribution mode is enabled.");
					return safe;
				}
				long sizeOfDirectory = FileUtils.sizeOfDirectory(dir);
				if (sizeOfDirectory > safeThreadHold) {
					perfTestService.markProgress(perfTest, "The total size of distributed files is over "
<<<<<<< HEAD
							+ safeThreadHold + "B.\n- Safe file distribution mode is enabled by force.");
=======
							+ UnitUtils.byteCountToDisplaySize(safeThreadHold) + "B.\n- Safe file distribution mode is enabled by force.");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					return true;
				}
				return safe;
			}

		});

		// the files have prepared before
<<<<<<< HEAD
		singleConsole.distributeFiles(perfTestService.getPerfTestDistributionPath(perfTest), listener,
=======
		singleConsole.distributeFiles(perfTestService.getDistributionPath(perfTest), listener,
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				isSafeDistPerfTest(perfTest));
		perfTestService.markStatusAndProgress(perfTest, DISTRIBUTE_FILES_FINISHED,
				"All necessary files are distributed.");
	}

<<<<<<< HEAD
	private int getSafeTransitionThreadHold() {
		// For backward compatibility
		int safeThreadHold = config.getSystemProperties().getPropertyInt(NGRINDER_PROP_DIST_SAFE_THRESHHOLD_OLD, 0);
		if (safeThreadHold == 0) {
			safeThreadHold = config.getSystemProperties().getPropertyInt(NGRINDER_PROP_DIST_SAFE_THRESHHOLD,
					1 * 1024 * 1024);
		}
		return safeThreadHold;
	}

	private boolean isSafeDistPerfTest(final PerfTest perfTest) {
		boolean safeDist = perfTest.getSafeDistribution();
		if (config.isCluster()) {
			String distSafeRegion = config.getSystemProperties().getProperty(NGRINDER_PROP_DIST_SAFE_REGION,
					StringUtils.EMPTY);
			for (String each : StringUtils.split(distSafeRegion, ",")) {
				if (StringUtils.equals(perfTest.getRegion(), StringUtils.trim(each))) {
					safeDist = true;
					break;
				}
			}
=======
	protected int getSafeTransmissionThreshold() {
		// For backward compatibility
		return config.getControllerProperties().getPropertyInt(PROP_CONTROLLER_SAFE_DIST_THRESHOLD);
	}

	private boolean isSafeDistPerfTest(final PerfTest perfTest) {
		boolean safeDist = getSafe(perfTest.getSafeDistribution());
		if (config.isClustered()) {
			safeDist = config.getClusterProperties().getPropertyBoolean(PROP_CLUSTER_SAFE_DIST);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
		return safeDist;
	}

	/**
	 * Start agents for the given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
	 * @param grinderProperties
	 *            grinder properties
	 * @param singleConsole
	 *            console to be used.
	 */
	void startAgentsOn(PerfTest perfTest, GrinderProperties grinderProperties, SingleConsole singleConsole) {
		perfTestService.markStatusAndProgress(perfTest, START_AGENTS, perfTest.getAgentCount()
				+ " agents are starting.");
		agentManager.runAgent(perfTest.getCreatedUser(), singleConsole, grinderProperties, perfTest.getAgentCount());
		singleConsole.waitUntilAgentConnected(perfTest.getAgentCount());
		perfTestService.markStatusAndProgress(perfTest, START_AGENTS_FINISHED, perfTest.getAgentCount()
=======
	 *
	 * @param perfTest          perftest
	 * @param grinderProperties grinder properties
	 * @param singleConsole     console to be used.
	 */
	void startAgentsOn(PerfTest perfTest, GrinderProperties grinderProperties, SingleConsole singleConsole) {
		perfTestService.markStatusAndProgress(perfTest, START_AGENTS, getSafe(perfTest.getAgentCount())
				+ " agents are starting.");
		agentManager.runAgent(perfTest.getCreatedUser(), singleConsole, grinderProperties,
				getSafe(perfTest.getAgentCount()));
		singleConsole.waitUntilAgentConnected(perfTest.getAgentCount());
		perfTestService.markStatusAndProgress(perfTest, START_AGENTS_FINISHED, getSafe(perfTest.getAgentCount())
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				+ " agents are ready.");
	}

	/**
	 * Run a given {@link PerfTest} with the given {@link GrinderProperties} and
	 * the {@link SingleConsole} .
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perftest
	 * @param grinderProperties
	 *            grinder properties
	 * @param singleConsole
	 *            console to be used.
=======
	 *
	 * @param perfTest          perftest
	 * @param grinderProperties grinder properties
	 * @param singleConsole     console to be used.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	void runTestOn(final PerfTest perfTest, GrinderProperties grinderProperties, final SingleConsole singleConsole) {
		// start target monitor
		for (OnTestLifeCycleRunnable run : pluginManager.getEnabledModulesByClass(OnTestLifeCycleRunnable.class)) {
<<<<<<< HEAD
			run.start(perfTest, perfTestService, config.getVesion());
		}

		addSamplingListeners(perfTest, singleConsole);

=======
			run.start(perfTest, perfTestService, config.getVersion());
		}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		// Run test
		perfTestService.markStatusAndProgress(perfTest, START_TESTING, "The test is ready to start.");
		// Add listener to detect abnormal condition and mark the perfTest
		singleConsole.addListener(new ConsoleShutdownListener() {
			@Override
			public void readyToStop(StopReason stopReason) {
<<<<<<< HEAD
				perfTestService.markAbromalTermination(perfTest, stopReason);
=======
				perfTestService.markAbnormalTermination(perfTest, stopReason);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				LOG.error("Abnormal test {} due to {}", perfTest.getId(), stopReason.name());
			}
		});
		long startTime = singleConsole.startTest(grinderProperties);
		perfTest.setStartTime(new Date(startTime));
<<<<<<< HEAD
		perfTestService.markStatusAndProgress(perfTest, TESTING, "The test is started.");
		singleConsole.startSampling(grinderProperties.getInt(GRINDER_PROP_IGNORE_SAMPLE_COUNT, 0));

	}

	private void addSamplingListeners(final PerfTest perfTest, final SingleConsole singleConsole) {
		// Add the SamplingLifeCycleFollowUpListener
		singleConsole.addSamplingLifeCycleFollowUpCycleListener(new MonitorCollectorListener(this.applicationContext,
				perfTest.getId(), createMonitorTargets(perfTest), singleConsole.getReportPath()));

		// Add SamplingLifeCycleListener
		singleConsole.addSamplingLifeCyleListener(new PerfTestSamplingCollectorListener(singleConsole,
				perfTest.getId(), perfTestService));
		singleConsole.addSamplingLifeCyleListener(new AgentLostDetectionListener(singleConsole, perfTest,
				perfTestService));
		singleConsole.addSamplingLifeCyleListener(new PluginRunListener(this.testSamplingRunnables, singleConsole,
				perfTest, perfTestService));
		singleConsole.addSamplingLifeCyleListener(new AgentDieHardListener(singleConsole, perfTest, perfTestService,
				agentManager));
	}

	private Set<AgentInfo> createMonitorTargets(final PerfTest perfTest) {
		final Set<AgentInfo> agents = Sets.newHashSet();
		Set<String> ipSet = Sets.newHashSet();
		List<String> targetIPList = perfTest.getTargetHostIP();
		for (String targetIP : targetIPList) {
			if (ipSet.contains(targetIP)) {
				continue;
			}
			AgentInfo targetServer = new AgentInfo();
			targetServer.setIp(targetIP);
			targetServer.setPort(config.getMonitorPort());
			agents.add(targetServer);
			ipSet.add(targetIP);
		}
		return agents;
	}

	/**
	 * Notify test finish to plugins.
	 * 
	 * @param perfTest
	 *            PerfTest
	 * @param reason
	 *            the reason of test finish..
	 * @see OnTestLifeCycleRunnable
	 */
	public void notifyFinsish(PerfTest perfTest, StopReason reason) {
		for (OnTestLifeCycleRunnable run : pluginManager.getEnabledModulesByClass(OnTestLifeCycleRunnable.class)) {
			run.finish(perfTest, reason.name(), perfTestService, config.getVesion());
=======
		addSamplingListeners(perfTest, singleConsole);
		perfTestService.markStatusAndProgress(perfTest, TESTING, "The test is started.");
		singleConsole.startSampling();

	}

	protected void addSamplingListeners(final PerfTest perfTest, final SingleConsole singleConsole) {
		// Add SamplingLifeCycleListener
		singleConsole.addSamplingLifeCyleListener(new PerfTestSamplingCollectorListener(singleConsole,
				perfTest.getId(), perfTestService, scheduledTaskService));
		singleConsole.addSamplingLifeCyleListener(new AgentLostDetectionListener(singleConsole, perfTest,
				perfTestService, scheduledTaskService));
		List<OnTestSamplingRunnable> testSamplingPlugins = pluginManager.getEnabledModulesByClass
				(OnTestSamplingRunnable.class, new MonitorCollectorPlugin(config, scheduledTaskService,
						perfTestService, perfTest.getId()));
		singleConsole.addSamplingLifeCyleListener(new PluginRunListener(testSamplingPlugins, singleConsole,
				perfTest, perfTestService));
		singleConsole.addSamplingLifeCyleListener(new AgentDieHardListener(singleConsole, perfTest, perfTestService,
				agentManager, scheduledTaskService));
	}


	/**
	 * Notify test finish to plugins.
	 *
	 * @param perfTest PerfTest
	 * @param reason   the reason of test finish..
	 * @see OnTestLifeCycleRunnable
	 */
	public void notifyFinish(PerfTest perfTest, StopReason reason) {
		for (OnTestLifeCycleRunnable run : pluginManager.getEnabledModulesByClass(OnTestLifeCycleRunnable.class)) {
			run.finish(perfTest, reason.name(), perfTestService, config.getVersion());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
<<<<<<< HEAD
	 * Finish the tests.(Scheduled by SpringTask)<br/>
	 * 
	 * There are three types of finish. <br/>
	 * 
	 * <ul>
	 * <li>Abnormal test finish : when TPS is too low or too many errors occurs</li>
	 * <li>User requested test finish : when user requested to finish test from
	 * the UI</li>
	 * <li>Normal test finish : when test goes over the planned duration and run
	 * count.</li>
	 * </ul>
	 */
	@Scheduled(fixedDelay = PERFTEST_TERMINATION_FREQUENCY_MILLISECONDS)
	public void finishTest() {
		for (PerfTest each : perfTestService.getAbnoramlTestingPerfTest()) {
			LOG.error("Terminate {}", each.getId());
			SingleConsole consoleUsingPort = consoleManager.getConsoleUsingPort(each.getPort());
			doTerminate(each, consoleUsingPort);
			cleanUp(each);
			notifyFinsish(each, StopReason.TOO_MANY_ERRORS);
		}

		for (PerfTest each : perfTestService.getStopRequestedPerfTest()) {
			LOG.error("Stop test {}", each.getId());
			SingleConsole consoleUsingPort = consoleManager.getConsoleUsingPort(each.getPort());
			doCancel(each, consoleUsingPort);
			cleanUp(each);
			notifyFinsish(each, StopReason.CANCEL_BY_USER);
		}

		for (PerfTest each : perfTestService.getTestingPerfTest()) {
			SingleConsole consoleUsingPort = consoleManager.getConsoleUsingPort(each.getPort());
			if (isTestFinishCandidate(each, consoleUsingPort)) {
				doFinish(each, consoleUsingPort);
				cleanUp(each);
				notifyFinsish(each, StopReason.NORMAL);
			}
		}

=======
	 * Finish the tests.(Scheduled by SpringTask)
	 * <p/>
	 * There are three types of test finish.
	 * <p/>
	 * <ul>
	 * <li>Abnormal test finish : when TPS is too low or too many errors occur</li>
	 * <li>User requested test finish : when user requested to finish the test</li>
	 * <li>Normal test finish : when the test reaches the planned duration and run
	 * count.</li>
	 * </ul>
	 */
	public void finishPeriodically() {
		doFinish(false);
	}

	protected void doFinish(boolean initial) {
		if (!initial && consoleManager.getConsoleInUse().isEmpty()) {
			return;
		}
		doFinish();
	}

	void doFinish() {
		for (PerfTest each : perfTestService.getAllAbnormalTesting()) {
			LOG.info("Terminate {}", each.getId());
			SingleConsole consoleUsingPort = consoleManager.getConsoleUsingPort(each.getPort());
			doTerminate(each, consoleUsingPort);
			cleanUp(each);
			notifyFinish(each, StopReason.TOO_MANY_ERRORS);
		}

		for (PerfTest each : perfTestService.getAllStopRequested()) {
			LOG.info("Stop test {}", each.getId());
			SingleConsole consoleUsingPort = consoleManager.getConsoleUsingPort(each.getPort());
			doCancel(each, consoleUsingPort);
			cleanUp(each);
			notifyFinish(each, StopReason.CANCEL_BY_USER);
		}

		for (PerfTest each : perfTestService.getAllTesting()) {
			SingleConsole consoleUsingPort = consoleManager.getConsoleUsingPort(each.getPort());
			if (isTestFinishCandidate(each, consoleUsingPort)) {
				doNormalFinish(each, consoleUsingPort);
				cleanUp(each);
				notifyFinish(each, StopReason.NORMAL);
			}
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Clean up distribution directory for the given perfTest.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perfTest
=======
	 *
	 * @param perfTest perfTest
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	private void cleanUp(PerfTest perfTest) {
		perfTestService.cleanUpDistFolder(perfTest);
		perfTestService.cleanUpRuntimeOnlyData(perfTest);
	}

	/**
	 * Check if the given {@link PerfTest} is ready to finish.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            perf test
	 * @param singleConsoleInUse
	 *            singleConsole
=======
	 *
	 * @param perfTest           perf test
	 * @param singleConsoleInUse singleConsole
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return true if it's a finish candidate.
	 */
	private boolean isTestFinishCandidate(PerfTest perfTest, SingleConsole singleConsoleInUse) {
		// Give 5 seconds to be finished
<<<<<<< HEAD
		if (perfTest.isThreshholdDuration()
				&& singleConsoleInUse.isCurrentRunningTimeOverDuration(perfTest.getDuration())) {
			LOG.debug(
					"Test {} is ready to finish. Current : {}, Planned : {}",
					new Object[] { perfTest.getTestIdentifier(), singleConsoleInUse.getCurrentRunningTime(),
							perfTest.getDuration() });
			return true;
		} else if (perfTest.isThreshholdRunCount()
				&& singleConsoleInUse.getCurrentExecutionCount() >= perfTest.getTotalRunCount()) {
			LOG.debug("Test {} is ready to finish. Current : {}, Planned : {}",
					new Object[] { perfTest.getTestIdentifier(), singleConsoleInUse.getCurrentExecutionCount(),
							perfTest.getTotalRunCount() });
			return true;
		} else if (singleConsoleInUse instanceof NullSingleConsole) {
			LOG.debug("Test {} is ready to finish. Current : {}, Planned : {}",
					new Object[] { perfTest.getTestIdentifier(), singleConsoleInUse.getCurrentExecutionCount(),
							perfTest.getTotalRunCount() });
=======
		if (perfTest.isThresholdDuration()
				&& singleConsoleInUse.isCurrentRunningTimeOverDuration(perfTest.getDuration())) {
			LOG.debug(
					"Test {} is ready to finish. Current : {}, Planned : {}",
					new Object[]{perfTest.getTestIdentifier(), singleConsoleInUse.getCurrentRunningTime(),
							perfTest.getDuration()});
			return true;
		} else if (perfTest.isThresholdRunCount()
				&& singleConsoleInUse.getCurrentExecutionCount() >= perfTest.getTotalRunCount()) {
			LOG.debug("Test {} is ready to finish. Current : {}, Planned : {}",
					new Object[]{perfTest.getTestIdentifier(), singleConsoleInUse.getCurrentExecutionCount(),
							perfTest.getTotalRunCount()});
			return true;
		} else if (singleConsoleInUse instanceof NullSingleConsole) {
			LOG.debug("Test {} is ready to finish. Current : {}, Planned : {}",
					new Object[]{perfTest.getTestIdentifier(), singleConsoleInUse.getCurrentExecutionCount(),
							perfTest.getTotalRunCount()});
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			return true;
		}

		return false;
	}

	/**
	 * Cancel the given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            {@link PerfTest} to be canceled.
	 * @param singleConsoleInUse
	 *            {@link SingleConsole} which is being used for the given
	 *            {@link PerfTest}
	 */
	public void doCancel(PerfTest perfTest, SingleConsole singleConsoleInUse) {
		LOG.error("Cancel {} by user request.", perfTest.getTestIdentifier());
=======
	 *
	 * @param perfTest           {@link PerfTest} to be canceled.
	 * @param singleConsoleInUse {@link SingleConsole} which is being used for the given
	 *                           {@link PerfTest}
	 */
	public void doCancel(PerfTest perfTest, SingleConsole singleConsoleInUse) {
		LOG.info("Cancel test {} by user request.", perfTest.getId());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		singleConsoleInUse.unregisterSampling();
		try {
			perfTestService.markProgressAndStatusAndFinishTimeAndStatistics(perfTest, CANCELED,
					"Stop requested by user");
		} catch (Exception e) {
<<<<<<< HEAD
			LOG.error("Error while canceling {} : {}", perfTest.getTestIdentifier(), e.getMessage());
=======
			LOG.error("Error while canceling test {} : {}", perfTest.getId(), e.getMessage());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			LOG.debug("Details : ", e);
		}
		consoleManager.returnBackConsole(perfTest.getTestIdentifier(), singleConsoleInUse);
	}

	/**
	 * Terminate the given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            {@link PerfTest} to be finished
	 * @param singleConsoleInUse
	 *            {@link SingleConsole} which is being used for the given
	 *            {@link PerfTest}
=======
	 *
	 * @param perfTest           {@link PerfTest} to be finished
	 * @param singleConsoleInUse {@link SingleConsole} which is being used for the given
	 *                           {@link PerfTest}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void doTerminate(PerfTest perfTest, SingleConsole singleConsoleInUse) {
		singleConsoleInUse.unregisterSampling();
		try {
			perfTestService.markProgressAndStatusAndFinishTimeAndStatistics(perfTest, Status.STOP_BY_ERROR,
<<<<<<< HEAD
					"Stoped by error");
=======
					"Stopped by error");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		} catch (Exception e) {
			LOG.error("Error while terminating {} : {}", perfTest.getTestIdentifier(), e.getMessage());
			LOG.debug("Details : ", e);
		}
		consoleManager.returnBackConsole(perfTest.getTestIdentifier(), singleConsoleInUse);
	}

	/**
	 * Finish the given {@link PerfTest}.
<<<<<<< HEAD
	 * 
	 * @param perfTest
	 *            {@link PerfTest} to be finished
	 * @param singleConsoleInUse
	 *            {@link SingleConsole} which is being used for the given
	 *            {@link PerfTest}
	 */
	public void doFinish(PerfTest perfTest, SingleConsole singleConsoleInUse) {
		// FIXME... it should found abnormal test status..
=======
	 *
	 * @param perfTest           {@link PerfTest} to be finished
	 * @param singleConsoleInUse {@link SingleConsole} which is being used for the given
	 *                           {@link PerfTest}
	 */
	public void doNormalFinish(PerfTest perfTest, SingleConsole singleConsoleInUse) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		LOG.debug("PerfTest {} status - currentRunningTime {} ", perfTest.getId(),
				singleConsoleInUse.getCurrentRunningTime());
		singleConsoleInUse.unregisterSampling();
		try {
			// stop target host monitor
			if (perfTestService.hasTooManError(perfTest)) {
				perfTestService.markProgressAndStatusAndFinishTimeAndStatistics(perfTest, Status.STOP_BY_ERROR,
						"[WARNING] The test is finished but contains too much errors(over 30% of total runs).");
			} else if (singleConsoleInUse.hasNoPerformedTest()) {
				perfTestService.markProgressAndStatusAndFinishTimeAndStatistics(perfTest, Status.STOP_BY_ERROR,
						"[WARNING] The test is finished but has no TPS.");
			} else {
				perfTestService.markProgressAndStatusAndFinishTimeAndStatistics(perfTest, Status.FINISHED,
						"The test is successfully finished.");
			}
		} catch (Exception e) {
			perfTestService.markStatusAndProgress(perfTest, Status.STOP_BY_ERROR, e.getMessage());
			LOG.error("Error while finishing {} : {}", perfTest.getTestIdentifier(), e.getMessage());
			LOG.debug("Details : ", e);
		}
		consoleManager.returnBackConsole(perfTest.getTestIdentifier(), singleConsoleInUse);
	}

	public PerfTestService getPerfTestService() {
		return perfTestService;
	}

	public AgentManager getAgentManager() {
		return agentManager;
	}

}
