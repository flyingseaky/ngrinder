<<<<<<< HEAD
/* 
=======
  /*
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
<<<<<<< HEAD
 * limitations under the License. 
 */
package org.ngrinder.infra.config;

import static org.ngrinder.common.util.Preconditions.checkNotNull;

import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import net.grinder.util.ListenerSupport;
import net.grinder.util.ListenerSupport.Informer;
import net.grinder.util.NetworkUtil;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.constant.NGrinderConstants;
import org.ngrinder.common.exception.ConfigurationException;
import org.ngrinder.common.model.Home;
import org.ngrinder.common.util.FileWatchdog;
import org.ngrinder.common.util.PropertiesWrapper;
import org.ngrinder.infra.AgentConfig;
import org.ngrinder.infra.logger.CoreLogger;
import org.ngrinder.monitor.MonitorConstants;
import org.ngrinder.service.IConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

/**
 * Spring component which is responsible to get the nGrinder configurations which is stored ${NGRINDER_HOME}.
 * 
=======
 * limitations under the License.
 */
package org.ngrinder.infra.config;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import net.grinder.util.ListenerSupport;
import net.grinder.util.ListenerSupport.Informer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.constant.ClusterConstants;
import org.ngrinder.common.constant.ControllerConstants;
import org.ngrinder.common.constants.InternalConstants;
import org.ngrinder.common.exception.ConfigurationException;
import org.ngrinder.common.model.Home;
import org.ngrinder.common.util.FileWatchdog;
import org.ngrinder.common.util.PropertiesKeyMapper;
import org.ngrinder.common.util.PropertiesWrapper;
import org.ngrinder.infra.logger.CoreLogger;
import org.ngrinder.infra.spring.SpringContext;
import org.ngrinder.service.AbstractConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;

import static net.grinder.util.NoOp.noOp;
import static org.ngrinder.common.constant.DatabaseConstants.PROP_DATABASE_UNIT_TEST;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

/**
 * Spring component which is responsible to get the nGrinder configurations which is stored ${NGRINDER_HOME}.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.0
 */
@Component
<<<<<<< HEAD
public class Config implements IConfig, NGrinderConstants {
=======
public class Config extends AbstractConfig implements ControllerConstants, ClusterConstants {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private static final String NGRINDER_DEFAULT_FOLDER = ".ngrinder";
	private static final String NGRINDER_EX_FOLDER = ".ngrinder_ex";
	private static final Logger LOG = LoggerFactory.getLogger(Config.class);
	private Home home = null;
	private Home exHome = null;
	private PropertiesWrapper internalProperties;
<<<<<<< HEAD
	private PropertiesWrapper systemProperties;
	private PropertiesWrapper databaseProperties;
	private String announcement;
	private Date announcementDate;
	private static String versionString = "";
	private boolean verbose;
	private String currentIP;

	public static final int NGRINDER_DEFAULT_CLUSTER_LISTENER_PORT = 40003;
=======
	private PropertiesWrapper controllerProperties;
	private PropertiesWrapper databaseProperties;
	private PropertiesWrapper clusterProperties;
	private String announcement = "";
	private Date announcementDate;
	private boolean verbose;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	public static final String NONE_REGION = "NONE";
	private boolean cluster;
	private ListenerSupport<PropertyChangeListener> systemConfListeners = new ListenerSupport<PropertyChangeListener>();

<<<<<<< HEAD
=======
	protected PropertiesKeyMapper internalPropertiesKeyMapper = PropertiesKeyMapper.create("internal-properties.map");
	protected PropertiesKeyMapper databasePropertiesKeyMapper = PropertiesKeyMapper.create("database-properties.map");
	protected PropertiesKeyMapper controllerPropertiesKeyMapper = PropertiesKeyMapper.create("controller-properties.map");
	protected PropertiesKeyMapper clusterPropertiesKeyMapper = PropertiesKeyMapper.create("cluster-properties.map");

	@SuppressWarnings("SpringJavaAutowiringInspection")
	@Autowired
	private SpringContext context;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	/**
	 * Make it singleton.
	 */
	Config() {
	}

	/**
	 * Add the system configuration change listener.
<<<<<<< HEAD
	 * 
	 * @param listener
	 *            listener
	 */
=======
	 *
	 * @param listener listener
	 */
	@SuppressWarnings("UnusedDeclaration")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public void addSystemConfListener(PropertyChangeListener listener) {
		systemConfListeners.add(listener);
	}

	/**
	 * Initialize the {@link Config} object.
<<<<<<< HEAD
	 * 
=======
	 * <p/>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * This method mainly resolves ${NGRINDER_HOME} and loads system properties. In addition, the logger is initialized
	 * and the default configuration files are copied into ${NGRINDER_HOME} if they do not exists.
	 */
	@PostConstruct
	public void init() {
		try {
<<<<<<< HEAD
			CoreLogger.LOGGER.info("NGrinder is starting...");
			home = resolveHome();
			exHome = resolveExHome();
			copyDefaultConfigurationFiles();
			loadIntrenalProperties();
			loadSystemProperties();
			initHomeMonitor();
			// Load cluster in advance. cluster mode is not dynamically
			// reloadable.
			cluster = getSystemProperties().getPropertyBoolean(NGrinderConstants.NGRINDER_PROP_CLUSTER_MODE, false);
			initLogger(isTestMode());
			resolveLocalIp();
			loadDatabaseProperties();
			setRMIHostName();
			versionString = getVesion();
=======
			CoreLogger.LOGGER.info("nGrinder is starting...");
			home = resolveHome();
			home.init();
			exHome = resolveExHome();
			copyDefaultConfigurationFiles();
			loadInternalProperties();
			loadProperties();
			initHomeMonitor();
			// Load cluster in advance. cluster mode is not dynamically
			// reloadable.
			cluster = resolveClusterMode();
			initDevModeProperties();
			loadAnnouncement();
			loadDatabaseProperties();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		} catch (IOException e) {
			throw new ConfigurationException("Error while init nGrinder", e);
		}
	}

<<<<<<< HEAD
	protected void resolveLocalIp() {
		currentIP = getSystemProperties().getPropertyWithBackwardCompatibility("ngrinder.controller.ip",
				"ngrinder.controller.ipaddress", NetworkUtil.getLocalHostAddress());
=======
	protected void initDevModeProperties() {
		if (!isDevMode()) {
			initLogger(false);
		} else {
			final PropertiesWrapper controllerProperties = getControllerProperties();
			controllerProperties.addProperty(PROP_CONTROLLER_AGENT_FORCE_UPDATE, "true");
			controllerProperties.addProperty(PROP_CONTROLLER_ENABLE_AGENT_AUTO_APPROVAL, "true");
			controllerProperties.addProperty(PROP_CONTROLLER_ENABLE_SCRIPT_CONSOLE, "true");
		}
	}

	private boolean resolveClusterMode() {
		String mode = getClusterProperties().getProperty(PROP_CLUSTER_MODE, "none");
		return !"none".equals(mode) || getClusterProperties().getPropertyBoolean(PROP_CLUSTER_ENABLED);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Destroy bean.
	 */
	@PreDestroy
	public void destroy() {
		// Stop all the non-daemon thread.
		announcementWatchDog.interrupt();
		systemConfWatchDog.interrupt();
		policyJsWatchDog.interrupt();
	}

	/**
<<<<<<< HEAD
	 * Set the RMI server host name.
	 * 
	 * @since 3.1
	 */
	protected void setRMIHostName() {
		if (isCluster()) {
			if (getRegion().equals(NONE_REGION)) {
				LOG.error("Region is not set in cluster mode. Please set ngrinder.region properly.");
			} else {
				CoreLogger.LOGGER.info("Cache cluster URIs:{}", getClusterURIs());
				// Set RMI server host for remote serving. Otherwise, maybe it
				// will use 127.0.0.1 as the RMI server name and the remote client can not connect.
				CoreLogger.LOGGER.info("Set current IP:{} for RMI server.", getCurrentIP());
				System.setProperty("java.rmi.server.hostname", getCurrentIP());
			}
		}
	}

	/**
	 * Get if the cluster mode is enable or not.
	 * 
	 * @return true if the cluster mode is enabled.
	 * @since 3.1
	 */
	public boolean isCluster() {
		return cluster;
	}

	/**
	 * Get the ngrinder instance IPs consisting of the current cluster from the configuration.
	 * 
	 * @return ngrinder instance IPs
	 */
	public String[] getClusterURIs() {
		String clusterUri = getSystemProperties().getProperty(NGRINDER_PROP_CLUSTER_URIS, "");
		return StringUtils.split(clusterUri, ";");
=======
	 * Get if the cluster mode is enable or not.
	 *
	 * @return true if the cluster mode is enabled.
	 * @since 3.1
	 */
	public boolean isClustered() {
		return cluster;
	}

	public int getControllerPort() {
		return getControllerProperties().getPropertyInt(ControllerConstants.PROP_CONTROLLER_CONTROLLER_PORT);
	}

	/**
	 * Get the ngrinder instance IPs consisting of the current cluster from the configuration.
	 *
	 * @return ngrinder instance IPs
	 */
	public String[] getClusterURIs() {
		String clusterUri = getClusterProperties().getProperty(PROP_CLUSTER_MEMBERS);
		return StringUtils.split(StringUtils.trimToEmpty(clusterUri), ",;");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the current region from the configuration.
<<<<<<< HEAD
	 * 
	 * @return region. If it's not clustered mode, return "NONE"
	 */
	public String getRegion() {
		return isCluster() ? getSystemProperties().getProperty(NGRINDER_PROP_REGION, NONE_REGION) : NONE_REGION;
=======
	 *
	 * @return region. If it's not clustered mode, return "NONE"
	 */
	public String getRegion() {
		return isClustered() ? getClusterProperties().getProperty(PROP_CLUSTER_REGION) : NONE_REGION;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the monitor listener port from the configuration.
<<<<<<< HEAD
	 * 
	 * @return monitor port
	 */
	public int getMonitorPort() {
		return getSystemProperties().getPropertyInt(AgentConfig.MONITOR_LISTEN_PORT,
				MonitorConstants.DEFAULT_MONITOR_PORT);
=======
	 *
	 * @return monitor port
	 */
	public int getMonitorPort() {
		return getControllerProperties().getPropertyInt(PROP_CONTROLLER_MONITOR_PORT);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if the periodic usage report is enabled.
<<<<<<< HEAD
	 * 
	 * @return true if enabled.
	 */
	public boolean isUsageReportEnabled() {
		return getSystemProperties().getPropertyBoolean(NGrinderConstants.NGRINER_PROP_USAGE_REPORT, true);
=======
	 *
	 * @return true if enabled.
	 */
	public boolean isUsageReportEnabled() {
		return getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_USAGE_REPORT);
	}

	/**
	 * Check if user self-registration is enabled.
	 *
	 * @return true if enabled.
	 */
	public boolean isSignUpEnabled() {
		return getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_ALLOW_SIGN_UP);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Initialize Logger.
<<<<<<< HEAD
	 * 
	 * @param forceToVerbose
	 *            true to force verbose logging.
	 */
	public synchronized void initLogger(boolean forceToVerbose) {
		setupLogger((forceToVerbose) ? true : getSystemProperties().getPropertyBoolean("verbose", false));
=======
	 *
	 * @param forceToVerbose true to force verbose logging.
	 */
	public synchronized void initLogger(boolean forceToVerbose) {
		setupLogger((forceToVerbose) || getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_VERBOSE));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Set up the logger.
<<<<<<< HEAD
	 * 
	 * @param verbose
	 *            verbose mode?
=======
	 *
	 * @param verbose verbose mode?
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	protected void setupLogger(boolean verbose) {
		this.verbose = verbose;
		final LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		final JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(context);
		context.reset();
		context.putProperty("LOG_LEVEL", verbose ? "DEBUG" : "INFO");
		File logbackConf = home.getSubFile("logback.xml");
		try {
			if (!logbackConf.exists()) {
				logbackConf = new ClassPathResource("/logback/logback-ngrinder.xml").getFile();
<<<<<<< HEAD
				if (exHome.exists() && isCluster()) {
=======
				if (exHome.exists() && isClustered()) {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					context.putProperty("LOG_DIRECTORY", exHome.getGlobalLogFile().getAbsolutePath());
					context.putProperty("SUFFIX", "_" + getRegion());
				} else {
					context.putProperty("SUFFIX", "");
					context.putProperty("LOG_DIRECTORY", home.getGlobalLogFile().getAbsolutePath());
				}
			}
			configurator.doConfigure(logbackConf);
		} catch (JoranException e) {
			CoreLogger.LOGGER.error(e.getMessage(), e);
		} catch (IOException e) {
			CoreLogger.LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * Copy the default files and create default directories to ${NGRINDER_HOME}.
<<<<<<< HEAD
	 * 
	 * @throws IOException
	 *             occurs when there is no such a files.
	 */
	protected void copyDefaultConfigurationFiles() throws IOException {
		checkNotNull(home);
		home.copyFrom(new ClassPathResource("ngrinder_home_template").getFile(), false);
		home.makeSubPath(PLUGIN_PATH);
		home.makeSubPath(PERF_TEST_PATH);
		home.makeSubPath(DOWNLOAD_PATH);
=======
	 *
	 * @throws IOException occurs when there is no such a files.
	 */
	protected void copyDefaultConfigurationFiles() throws IOException {
		checkNotNull(home);
		home.copyFrom(new ClassPathResource("ngrinder_home_template").getFile());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Resolve nGrinder home path.
<<<<<<< HEAD
	 * 
	 * @return resolved home
	 */
	protected Home resolveHome() {
=======
	 *
	 * @return resolved home
	 */
	protected Home resolveHome() {
		if (StringUtils.isNotBlank(System.getProperty("unit-test"))) {
			final String tempDir = System.getProperty("java.io.tmpdir");
			final File tmpHome = new File(tempDir, ".ngrinder");
			if (tmpHome.mkdirs()) {
				LOG.info("{} is created", tmpHome.getPath());
			}
			try {
				FileUtils.forceDeleteOnExit(tmpHome);
			} catch (IOException e) {
				LOG.error("Error while setting forceDeleteOnExit on {}", tmpHome);
			}
			return new Home(tmpHome);
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		String userHomeFromEnv = System.getenv("NGRINDER_HOME");
		String userHomeFromProperty = System.getProperty("ngrinder.home");
		if (!StringUtils.equals(userHomeFromEnv, userHomeFromProperty)) {
			CoreLogger.LOGGER.warn("The path to ngrinder-home is ambiguous:");
			CoreLogger.LOGGER.warn("    System Environment:  NGRINDER_HOME=" + userHomeFromEnv);
<<<<<<< HEAD
			CoreLogger.LOGGER.warn("    Java Sytem Property:  ngrinder.home=" + userHomeFromProperty);
			CoreLogger.LOGGER.warn("    '" + userHomeFromProperty + "' is accepted.");
		}
		String userHome = StringUtils.defaultIfEmpty(userHomeFromProperty, userHomeFromEnv);
		File homeDirectory = (StringUtils.isNotEmpty(userHome)) ? new File(userHome) : new File(
				System.getProperty("user.home"), NGRINDER_DEFAULT_FOLDER);
		CoreLogger.LOGGER.info("nGrinder home directory:{}.", userHome);
=======
			CoreLogger.LOGGER.warn("    Java System Property:  ngrinder.home=" + userHomeFromProperty);
			CoreLogger.LOGGER.warn("    '" + userHomeFromProperty + "' is accepted.");
		}
		String userHome = StringUtils.defaultIfEmpty(userHomeFromProperty, userHomeFromEnv);
		if (StringUtils.isEmpty(userHome)) {
			userHome = System.getProperty("user.home") + File.separator + NGRINDER_DEFAULT_FOLDER;
		} else if (StringUtils.startsWith(userHome, "~" + File.separator)) {
			userHome = System.getProperty("user.home") + File.separator + userHome.substring(2);
		} else if (StringUtils.startsWith(userHome, "." + File.separator)) {
			userHome = System.getProperty("user.dir") + File.separator + userHome.substring(2);
		}

		userHome = FilenameUtils.normalize(userHome);
		File homeDirectory = new File(userHome);
		CoreLogger.LOGGER.info("nGrinder home directory:{}.", homeDirectory.getPath());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

		return new Home(homeDirectory);
	}

	/**
	 * Resolve nGrinder extended home path.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return resolved home
	 */
	protected Home resolveExHome() {
		String exHomeFromEnv = System.getenv("NGRINDER_EX_HOME");
<<<<<<< HEAD
		String exHomeFromProperty = System.getProperty("ngrinder.exhome");
		if (!StringUtils.equals(exHomeFromEnv, exHomeFromProperty)) {
			CoreLogger.LOGGER.warn("The path to ngrinder-exhome is ambiguous:");
			CoreLogger.LOGGER.warn("    System Environment:  NGRINDER_EX_HOME=" + exHomeFromEnv);
			CoreLogger.LOGGER.warn("    Java Sytem Property:  ngrinder.exhome=" + exHomeFromProperty);
			CoreLogger.LOGGER.warn("    '" + exHomeFromProperty + "' is accepted.");
		}
		String userHome = StringUtils.defaultIfEmpty(exHomeFromProperty, exHomeFromEnv);
		File exHomeDirectory = (StringUtils.isNotEmpty(userHome)) ? new File(userHome) : new File(
				System.getProperty("user.home"), NGRINDER_EX_FOLDER);
		CoreLogger.LOGGER.info("nGrinder ex home directory:{}.", exHomeDirectory);

=======
		String exHomeFromProperty = System.getProperty("ngrinder.ex_home");
		if (!StringUtils.equals(exHomeFromEnv, exHomeFromProperty)) {
			CoreLogger.LOGGER.warn("The path to ngrinder ex home is ambiguous:");
			CoreLogger.LOGGER.warn("    System Environment:  NGRINDER_EX_HOME=" + exHomeFromEnv);
			CoreLogger.LOGGER.warn("    Java System Property:  ngrinder.ex_home=" + exHomeFromProperty);
			CoreLogger.LOGGER.warn("    '" + exHomeFromProperty + "' is accepted.");
		}
		String userHome = StringUtils.defaultIfEmpty(exHomeFromProperty, exHomeFromEnv);

		if (StringUtils.isEmpty(userHome)) {
			userHome = System.getProperty("user.home") + File.separator + NGRINDER_EX_FOLDER;
		} else if (StringUtils.startsWith(userHome, "~" + File.separator)) {
			userHome = System.getProperty("user.home") + File.separator + userHome.substring(2);
		} else if (StringUtils.startsWith(userHome, "." + File.separator)) {
			userHome = System.getProperty("user.dir") + File.separator + userHome.substring(2);
		}

		userHome = FilenameUtils.normalize(userHome);
		File exHomeDirectory = new File(userHome);
		CoreLogger.LOGGER.info("nGrinder ex home directory:{}.", exHomeDirectory);
		try {
			//noinspection ResultOfMethodCallIgnored
			exHomeDirectory.mkdirs();
		} catch (Exception e) {
			// If it's not possible.. do it... without real directory.
			noOp();
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return new Home(exHomeDirectory, false);
	}

	/**
	 * Load internal properties which is not modifiable by user.
	 */
<<<<<<< HEAD
	protected void loadIntrenalProperties() {
=======
	protected void loadInternalProperties() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		InputStream inputStream = null;
		Properties properties = new Properties();
		try {
			inputStream = new ClassPathResource("/internal.properties").getInputStream();
			properties.load(inputStream);
<<<<<<< HEAD
			internalProperties = new PropertiesWrapper(properties);
		} catch (IOException e) {
			CoreLogger.LOGGER.error("Error while load internal.properties", e);
			internalProperties = new PropertiesWrapper(properties);
=======
			internalProperties = new PropertiesWrapper(properties, internalPropertiesKeyMapper);
		} catch (IOException e) {
			CoreLogger.LOGGER.error("Error while load internal.properties", e);
			internalProperties = new PropertiesWrapper(properties, internalPropertiesKeyMapper);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}

	/**
	 * Load database related properties. (database.conf)
	 */
	protected void loadDatabaseProperties() {
		checkNotNull(home);
		Properties properties = home.getProperties("database.conf");
		properties.put("NGRINDER_HOME", home.getDirectory().getAbsolutePath());
<<<<<<< HEAD
		databaseProperties = new PropertiesWrapper(properties);
=======
		properties.putAll(System.getProperties());
		databaseProperties = new PropertiesWrapper(properties, databasePropertiesKeyMapper);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Load system related properties. (system.conf)
	 */
<<<<<<< HEAD
	public synchronized void loadSystemProperties() {
		checkNotNull(home);
		Properties properties = home.getProperties("system.conf");
		properties.put("NGRINDER_HOME", home.getDirectory().getAbsolutePath());
		// Override if exists
		if (exHome.exists()) {
			Properties exProperties = exHome.getProperties("system-ex.conf");
			properties.putAll(exProperties);
		}
		systemProperties = new PropertiesWrapper(properties);
=======
	public synchronized void loadProperties() {
		Properties properties = checkNotNull(home).getProperties("system.conf");
		properties.put("NGRINDER_HOME", home.getDirectory().getAbsolutePath());
		if (exHome.exists()) {
			Properties exProperties = exHome.getProperties("system-ex.conf");
			if (exProperties.isEmpty()) {
				exProperties = exHome.getProperties("system.conf");
			}
			properties.putAll(exProperties);
		}
		properties.putAll(System.getProperties());
		// Override if exists
		controllerProperties = new PropertiesWrapper(properties, controllerPropertiesKeyMapper);
		clusterProperties = new PropertiesWrapper(properties, clusterPropertiesKeyMapper);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Load the announcement content.
	 */
<<<<<<< HEAD
	public synchronized void loadAnnouncement() {
		checkNotNull(home);
		File sysFile = home.getSubFile("announcement.conf");
		try {
			announcement = FileUtils.readFileToString(sysFile, "UTF-8");
			if (sysFile.exists()) {
				announcementDate = new Date(sysFile.lastModified());
			} else {
				announcementDate = null;
			}
			return;
		} catch (IOException e) {
			CoreLogger.LOGGER.error("Error while reading announcement file.", e);
			announcement = "";
		}
	}

	/** watch docs. */
=======
	@SuppressWarnings("SynchronizeOnNonFinalField")
	public void loadAnnouncement() {
		checkNotNull(home);
		synchronized (announcement) {
			File sysFile = home.getSubFile("announcement.conf");
			try {
				announcement = FileUtils.readFileToString(sysFile, "UTF-8");
				if (sysFile.exists()) {
					announcementDate = new Date(sysFile.lastModified());
				} else {
					announcementDate = null;
				}
			} catch (IOException e) {
				CoreLogger.LOGGER.error("Error while reading announcement file.", e);
				announcement = "";
			}
		}
	}

	/**
	 * watch docs.
	 */
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private FileWatchdog announcementWatchDog;
	private FileWatchdog systemConfWatchDog;
	private FileWatchdog policyJsWatchDog;

<<<<<<< HEAD
	private void initHomeMonitor() {
=======
	protected void initHomeMonitor() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		checkNotNull(home);
		this.announcementWatchDog = new FileWatchdog(getHome().getSubFile("announcement.conf").getAbsolutePath()) {
			@Override
			protected void doOnChange() {
				CoreLogger.LOGGER.info("Announcement file is changed.");
				loadAnnouncement();
			}
		};
<<<<<<< HEAD
		announcementWatchDog.setName("WatchDog - annoucenment.conf");
=======
		announcementWatchDog.setName("WatchDog - announcement.conf");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		announcementWatchDog.setDelay(2000);
		announcementWatchDog.start();
		this.systemConfWatchDog = new FileWatchdog(getHome().getSubFile("system.conf").getAbsolutePath()) {
			@Override
			protected void doOnChange() {
				try {
					CoreLogger.LOGGER.info("System configuration(system.conf) is changed.");
<<<<<<< HEAD
					loadSystemProperties();
					resolveLocalIp();
=======
					loadProperties();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					systemConfListeners.apply(new Informer<PropertyChangeListener>() {
						@Override
						public void inform(PropertyChangeListener listener) {
							listener.propertyChange(null);
						}
					});
					CoreLogger.LOGGER.info("New system configuration is applied.");
				} catch (Exception e) {
					CoreLogger.LOGGER.error("Error occurs while applying new system configuration", e);
				}

			}
		};
		systemConfWatchDog.setName("WatchDoc - system.conf");
		systemConfWatchDog.setDelay(2000);
		systemConfWatchDog.start();

		String processThreadPolicyPath = getHome().getSubFile("process_and_thread_policy.js").getAbsolutePath();
		this.policyJsWatchDog = new FileWatchdog(processThreadPolicyPath) {
			@Override
			protected void doOnChange() {
				CoreLogger.LOGGER.info("process_and_thread_policy file is changed.");
				policyScript = "";
			}
		};
		policyJsWatchDog.setName("WatchDoc - process_and_thread_policy.js");
		policyJsWatchDog.setDelay(2000);
		policyJsWatchDog.start();
	}

	/**
	 * Get the database properties.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return database properties
	 */
	public PropertiesWrapper getDatabaseProperties() {
		checkNotNull(databaseProperties);
<<<<<<< HEAD
=======
		if (context.isUnitTestContext() && !databaseProperties.exist(PROP_DATABASE_UNIT_TEST)) {
			databaseProperties.addProperty(PROP_DATABASE_UNIT_TEST, "true");
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return databaseProperties;
	}

	/**
	 * Check if it's test mode.
<<<<<<< HEAD
	 * 
	 * @return true if test mode
	 */
	public boolean isTestMode() {
		return getSystemProperties().getPropertyBoolean("testmode", false);
=======
	 *
	 * @return true if test mode
	 */
	public boolean isDevMode() {
		return getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_DEV_MODE);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if the user security is enabled.
<<<<<<< HEAD
	 * 
	 * @return true if user security is enabled.
	 */
	public boolean isUserSecurityEnabled() {
		return getSystemProperties().getPropertyBoolean("user.security", true);
=======
	 *
	 * @return true if user security is enabled.
	 */
	public boolean isUserSecurityEnabled() {
		return getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_USER_SECURITY);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if it's the security enabled mode.
<<<<<<< HEAD
	 * 
	 * @return true if security is enabled.
	 */
	public boolean isSecurityEnabled() {
		return !isTestMode() && getSystemProperties().getPropertyBoolean("security", false);
=======
	 *
	 * @return true if security is enabled.
	 */
	public boolean isSecurityEnabled() {
		return !isDevMode() && getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_SECURITY);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if it is the demo mode.
<<<<<<< HEAD
	 * 
	 * @return true if demo mode is enabled.
	 */
	public boolean isDemo() {
		return getSystemProperties().getPropertyBoolean("demo", false);
=======
	 *
	 * @return true if demo mode is enabled.
	 */
	public boolean isDemo() {
		return getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_DEMO_MODE);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if the plugin support is enabled.
<<<<<<< HEAD
	 * 
	 * The reason why we need this configuration is that it takes time to initialize plugin system in unit test context.
	 * 
	 * @return true if the plugin is supported.
	 */
	public boolean isPluginSupported() {
		return !isTestMode() && (getSystemProperties().getPropertyBoolean("pluginsupport", true));
=======
	 * <p/>
	 * The reason why we need this configuration is that it takes time to initialize plugin system in unit test context.
	 *
	 * @return true if the plugin is supported.
	 */
	public boolean isPluginSupported() {
		return (getControllerProperties().getPropertyBoolean(PROP_CONTROLLER_PLUGIN_SUPPORT));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the resolved home folder.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return home
	 */
	public Home getHome() {
		return this.home;
	}

	/**
	 * Get the resolved extended home folder.
<<<<<<< HEAD
	 * 
	 * @since 3.1
	 * @return home
=======
	 *
	 * @return home
	 * @since 3.1
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public Home getExHome() {
		return this.exHome;
	}

	/**
	 * Get the system properties.
<<<<<<< HEAD
	 * 
	 * @return {@link PropertiesWrapper} which is loaded from system.conf.
	 */
	public PropertiesWrapper getSystemProperties() {
		return checkNotNull(systemProperties);
=======
	 *
	 * @return {@link PropertiesWrapper} which is loaded from system.conf.
	 */
	public PropertiesWrapper getControllerProperties() {
		return checkNotNull(controllerProperties);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the announcement content.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return loaded from announcement.conf.
	 */
	public String getAnnouncement() {
		return announcement;
	}

	/**
	 * Get the nGrinder version number.
<<<<<<< HEAD
	 * 
	 * @return nGrinder version number. If not set, return "0.0.1"
	 */
	public String getVesion() {
		return getInternalProperties().getProperty("ngrinder.version", "0.0.1");
=======
	 *
	 * @return version number
	 */
	public String getVersion() {
		return getInternalProperties().getProperty(InternalConstants.PROP_INTERNAL_NGRINDER_VERSION);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Policy file which is used to determine the count of processes and threads.
	 */
	private String policyScript = "";

	/**
	 * Get the content of "process_and_thread_policy.js" file.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return loaded file content.
	 */
	public String getProcessAndThreadPolicyScript() {
		if (StringUtils.isEmpty(policyScript)) {
			try {
				policyScript = FileUtils.readFileToString(getHome().getSubFile("process_and_thread_policy.js"));
				return policyScript;
			} catch (IOException e) {
				LOG.error("Error while load process_and_thread_policy.js", e);
			}
		}
		return policyScript;
	}

	/**
	 * Get the internal properties.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return internal properties
	 */
	public PropertiesWrapper getInternalProperties() {
		return internalProperties;
	}

<<<<<<< HEAD
	/**
	 * Get nGrinder version in static way.
	 * 
	 * @return nGrinder version.
	 */
	public static String getVerionString() {
		return versionString;
	}

	/**
	 * Check if it's verbose logging mode.
	 * 
=======

	/**
	 * Check if it's verbose logging mode.
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return true if verbose
	 */
	public boolean isVerbose() {
		return verbose;
	}

	/**
	 * Get the currently configured controller IP.
<<<<<<< HEAD
	 * 
	 * @return current IP.
	 */
	public String getCurrentIP() {
		return currentIP;
	}

	/**
	 * Check if the current ngrinder instance is hidden instance from the cluster.
	 * 
	 * @return true if hidden.
	 */
	public boolean isInvisibleRegion() {
		return getSystemProperties().getPropertyBoolean(NGRINDER_PROP_REGION_HIDE, false);
=======
	 *
	 * @return current IP.
	 */
	public String getCurrentIP() {
		if (cluster) {
			return StringUtils.trimToEmpty(getClusterProperties().getProperty(PROP_CLUSTER_HOST));
		} else {
			return StringUtils.trimToEmpty(getControllerProperties().getProperty(PROP_CONTROLLER_IP));
		}
	}


	/**
	 * Check if the current ngrinder instance is hidden instance from the cluster.
	 *
	 * @return true if hidden.
	 */
	public boolean isInvisibleRegion() {
		return getClusterProperties().getPropertyBoolean(PROP_CLUSTER_HIDDEN_REGION);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if no_more_test.lock to block further test executions exists.
<<<<<<< HEAD
	 * 
	 * @return true if it exists
	 */
	public boolean hasNoMoreTestLock() {
		if (exHome.exists()) {
			return exHome.getSubFile("no_more_test.lock").exists();
		}
		return false;
=======
	 *
	 * @return true if it exists
	 */
	public boolean hasNoMoreTestLock() {
		return exHome.exists() && exHome.getSubFile("no_more_test.lock").exists();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Check if shutdown.lock exists.
<<<<<<< HEAD
	 * 
	 * @return true if it exists
	 */
	public boolean hasShutdownLock() {
		if (exHome.exists()) {
			return exHome.getSubFile("shutdown.lock").exists();
		}
		return false;
=======
	 *
	 * @return true if it exists
	 */
	public boolean hasShutdownLock() {
		return exHome.exists() && exHome.getSubFile("shutdown.lock").exists();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Get the date of the recent announcement modification.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return the date of the recent announcement modification.
	 */
	public Date getAnnouncementDate() {
		return announcementDate;
	}

	/**
	 * Get ngrinder help URL.
<<<<<<< HEAD
	 * 
	 * @return help URL
	 */
	public String getHelpUrl() {
		return getSystemProperties().getProperty("ngrinder.help.url",
				"http://www.cubrid.org/wiki_ngrinder/entry/user-guide");
	}

=======
	 *
	 * @return help URL
	 */
	public String getHelpUrl() {
		return getControllerProperties().getProperty(PROP_CONTROLLER_HELP_URL);
	}

	public PropertiesWrapper getClusterProperties() {
		return clusterProperties;
	}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
