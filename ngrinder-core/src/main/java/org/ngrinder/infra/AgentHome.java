/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.infra;

<<<<<<< HEAD
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.NoOp.noOp;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Properties;

=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * Class which represents AgentHome.
 * 
 * @author JunHo Yoon
 * @since 3.0
 * 
=======
import java.io.*;
import java.util.Properties;

import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.NoOp.noOp;
import static org.ngrinder.common.util.Preconditions.checkNotNull;

/**
 * Class which represents AgentHome.
 *
 * @author JunHo Yoon
 * @since 3.0
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 */
public class AgentHome {

	private final File directory;
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentHome.class);

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param directory
	 *            agent home directory
=======
	 *
	 * @param directory agent home directory
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public AgentHome(File directory) {
		checkNotNull(directory, "The directory should not be null.");
		if (StringUtils.contains(directory.getAbsolutePath().trim(), " ")) {
			throw processException(String.format(
					"nGrinder agent home directory \"%s\" should not contain space."
							+ "Please set NGRINDER_AGENT_HOME env var in the different location",
					directory.getAbsolutePath()));
		}

		if (!directory.exists() && !directory.mkdirs()) {
			throw processException(String.format(
					"nGrinder agent home directory %s is not created. Please check the permission",
					directory.getAbsolutePath()));
		}

		if (!directory.isDirectory()) {
			throw processException(String.format(
<<<<<<< HEAD
					"nGrinder home directory %s is not directory. Please delete this file in advance",
=======
					"nGrinder home directory %s is not directory. Please delete this sourceFile in advance",
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					directory.getAbsolutePath()));
		}

		if (!directory.canWrite()) {
			throw processException(String.format(
					"nGrinder home directory %s is not writable. Please adjust permission on this folder", directory));
		}

		this.directory = directory;
	}

	/**
<<<<<<< HEAD
	 * Get agent home directory.
	 * 
=======
	 * Get the agent home directory.
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return agent home directory
	 */
	public File getDirectory() {
		return directory;
	}

	/**
<<<<<<< HEAD
	 * Copy {@link InputStream} to path in the target.
	 * 
	 * @param io
	 *            {@link InputStream}
	 * @param target
	 *            target path. only file name will be used.
	 * @param overwrite
	 *            true if overwrite
	 */
	public void copyFileTo(InputStream io, File target, boolean overwrite) {
		// Copy missing files
		try {
			target = new File(directory, target.getName());
			if (!(target.exists())) {
				FileUtils.writeByteArrayToFile(target, IOUtils.toByteArray(io));
			}
		} catch (IOException e) {
			throw processException("Failed to write a file to " + target.getAbsolutePath(), e);
=======
	 * Get agent native directory.
	 *
	 * @return agent native directory
	 */
	public File getNativeDirectory() {
		return mkDir(getFile("native"));
	}

	public File mkDir(File file) {
		if (!file.exists()) {
			if (file.mkdirs()) {
				LOGGER.info("{} is created.", file.getPath());
			}
		}
		return file;
	}

	/**
	 * Get temp directory.
	 *
	 * @return temp directory
	 */
	public File getTempDirectory() {
		return mkDir(getFile("temp"));
	}

	/**
	 * Copy the {@link File} to path in the home.
	 *
	 * @param sourceFile {@link File}
	 * @param target     target path. only sourceFile name will be used.
	 */
	public void copyFileTo(File sourceFile, String target) {
		// Copy missing files
		File targetFile = new File(directory, target);
		try {
			FileUtils.copyFile(sourceFile, targetFile);
		} catch (IOException e) {
			throw processException("Failed to write a sourceFile to " + target, e);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
<<<<<<< HEAD
	 * Get properties from path.
	 * 
	 * @param path
	 *            property file path
=======
	 * Write the content to path in the home.
	 *
	 * @param content {@link File}
	 * @param target  target path. only sourceFile name will be used.
	 */
	public void writeFileTo(String content, String target) {
		File targetFile = new File(directory, target);
		try {
			FileUtils.write(targetFile, content);
		} catch (IOException e) {
			throw processException("Failed to write a sourceFile to " + target, e);
		}
	}

	/**
	 * Get the properties from path.
	 *
	 * @param path property sourceFile path
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link Properties} instance. return empty property if it has
	 *         problem.
	 */
	public Properties getProperties(String path) {
		Properties properties = new Properties();
<<<<<<< HEAD
		InputStream is = null;
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		try {
			File propertiesFile = new File(directory, path);
			String config = FileUtils.readFileToString(propertiesFile, "UTF-8");
			properties.load(new StringReader(config));
		} catch (IOException e) {
			noOp();
<<<<<<< HEAD
		} finally {
			IOUtils.closeQuietly(is);
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
		return properties;

	}

	/**
<<<<<<< HEAD
	 * Get file from path.
	 * 
	 * @param path
	 *            path
=======
	 * Get the sourceFile from the given path.
	 *
	 * @param path path
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link File} instance.
	 */
	public File getFile(String path) {
		return new File(getDirectory(), path);
	}

	/**
	 * Save properties.
<<<<<<< HEAD
	 * 
	 * @param path
	 *            path to save
	 * @param properties
	 *            properties.
=======
	 *
	 * @param path       path to save
	 * @param properties properties.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void saveProperties(String path, Properties properties) {
		OutputStream out = null;
		try {
			File propertiesFile = new File(getDirectory(), path);
			out = FileUtils.openOutputStream(propertiesFile);
			properties.store(out, null);
		} catch (IOException e) {
<<<<<<< HEAD
			LOGGER.error("Could not save property  file on " + path, e);
=======
			LOGGER.error("Could not save property  sourceFile on " + path, e);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

	public File getLogDirectory() {
		return new File(getDirectory(), "log");
	}
}
