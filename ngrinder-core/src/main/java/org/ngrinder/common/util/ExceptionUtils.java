package org.ngrinder.common.util;

<<<<<<< HEAD
import java.util.List;

import org.ngrinder.common.exception.NGrinderRuntimeException;

=======
import org.ngrinder.common.exception.NGrinderRuntimeException;

import java.util.List;

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
/**
 * Exception processing utility.
 * 
 * @author junoyoon
 * @since 3.2.3
 */
<<<<<<< HEAD
=======
@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
public abstract class ExceptionUtils {
	/**
	 * Check if the exception is {@link NGrinderRuntimeException}. If so, throw it. If
	 * not, wrap the given exception and throw it.
	 * 
<<<<<<< HEAD
	 * @param t
	 *            Throwable
=======
	 * @param t 	Throwable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return exception
	 */
	public static NGrinderRuntimeException processException(Throwable t) {
		if (t instanceof NGrinderRuntimeException) {
			throw (NGrinderRuntimeException) sanitize(t);
		} else {
			throw new NGrinderRuntimeException(sanitize(t), true);
		}
	}

	/**
	 * Check if the exception {@link NGrinderRuntimeException}. If so, throw. If
	 * not, wrap the given exception.
	 * 
<<<<<<< HEAD
	 * @param message
	 *            message
=======
	 * @param message	message
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return exception
	 */
	public static NGrinderRuntimeException processException(String message) {
		throw processException(new NGrinderRuntimeException(message));
	}

	/**
	 * Check if the exception is {@link NGrinderRuntimeException}. If so, throw.
	 * If not, wrap the given exception.
	 * 
<<<<<<< HEAD
	 * @param message
	 *            message
	 * @param t
	 *            Throwable
=======
	 * @param message	message
	 * @param t 		Throwable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return exception
	 */
	public static NGrinderRuntimeException processException(String message, Throwable t) {
		if (t instanceof NGrinderRuntimeException) {
			throw (NGrinderRuntimeException) sanitize(t);
		} else {
			throw new NGrinderRuntimeException(message, sanitize(t), true);
		}
	}

	/**
	 * Filter the stacktrace elements with only interesting one.
	 * 
<<<<<<< HEAD
	 * @param throwable
	 *            throwable
=======
	 * @param throwable	throwable
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link Throwable} instance with interested stacktrace elements.
	 */
	public static Throwable sanitize(Throwable throwable) {
		if (throwable instanceof NGrinderRuntimeException) {
			if (((NGrinderRuntimeException) throwable).isSanitized()) {
				return throwable;
			}
		}
		Throwable t = throwable;
		while (t != null) {
			// Note that this getBoolean access may well be synced...
			StackTraceElement[] trace = t.getStackTrace();
			List<StackTraceElement> newTrace = CollectionUtils.newArrayList();
			for (StackTraceElement stackTraceElement : trace) {
				if (isApplicationClass(stackTraceElement.getClassName())) {
					newTrace.add(stackTraceElement);
				}
			}
			StackTraceElement[] clean = new StackTraceElement[newTrace.size()];
			newTrace.toArray(clean);
			t.setStackTrace(clean);
			t = t.getCause();
		}
		if (throwable instanceof NGrinderRuntimeException) {
			((NGrinderRuntimeException) throwable).setSanitized(true);
		}
		return throwable;
	}

	/**
	 * Check if the given class name is the application class or not.
	 * 
<<<<<<< HEAD
	 * @param className
	 *            class name including package name
=======
	 * @param className	class name including package name
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return true if application class
	 */
	private static boolean isApplicationClass(String className) {
		for (String each : getUninterestingPackages()) {
			if (className.startsWith(each)) {
				return false;
			}
		}
		return true;
	}

	private static final String[] NON_NGRINDER_PACKAGE = ("org.springframework.," + "javax.," + "org.apache.catalina.,"
			+ "sun.," + "net.sf.," + "java.," + "org.ngrinder.common.exception.NGrinderRuntimeException,"
			+ "com.springsource.," + "org.apache.coyote.," + "org.apache.tomact.,"
			+ "org.ngrinder.common.util.ExceptionUtils.").split("(\\s|,)+");

	/**
	 * Get interesting packages.
	 * 
	 * @return interesting packages
	 */
	protected static String[] getUninterestingPackages() {
		return NON_NGRINDER_PACKAGE;
	}

}
