/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder;

<<<<<<< HEAD
import static org.ngrinder.common.util.ExceptionUtils.processException;
import static org.ngrinder.common.util.Preconditions.checkNotNull;
import net.grinder.common.GrinderException;
import net.grinder.common.GrinderProperties;
import net.grinder.communication.AgentControllerCommunicationDefauts;
=======
import net.grinder.common.GrinderException;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import net.grinder.engine.agent.Agent;
import net.grinder.util.ListenerHelper;
import net.grinder.util.ListenerSupport;
import net.grinder.util.ListenerSupport.Informer;
import net.grinder.util.thread.Condition;
<<<<<<< HEAD

import org.ngrinder.common.util.ThreadUtil;
=======
import org.ngrinder.common.util.ThreadUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.infra.AgentConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * This is daemon wrapper for agent controller.
 * 
=======
import static org.ngrinder.common.util.ExceptionUtils.processException;

/**
 * This is daemon wrapper for agent controller.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 */
public class AgentControllerDaemon implements Agent {

	private static final int LOG_FREQUENCY = 5;
	private final AgentController agentController;
	private Thread thread;
<<<<<<< HEAD
	private final GrinderProperties properties;
	private final ListenerSupport<AgentControllerShutDownListener> m_listeners = ListenerHelper.create();
	private boolean forceToshutdown = false;
	// event synchronization for
	private Condition m_eventSyncCondition = new Condition();

	public static final Logger LOGGER = LoggerFactory.getLogger("agent controller daemon");
	/**
	 * Region of grinder agent.
	 */
	private String region = "";
	private AgentConfig agentConfig;

	/**
	 * Constructor.
	 * 
	 * @param currentIp
	 *            current ip
	 */
	public AgentControllerDaemon(String currentIp) {
		try {
			properties = new GrinderProperties(GrinderProperties.DEFAULT_PROPERTIES);
			agentController = new AgentController(m_eventSyncCondition, currentIp);
		} catch (GrinderException e) {
			throw processException("Exception occurred while initiating the agent controller deamon", e);
		}
	}

	/**
	 * Run agent controller with the default agent controller port.
	 * 
	 */
	public void run() {
		run(null, AgentControllerCommunicationDefauts.DEFAULT_AGENT_CONTROLLER_SERVER_PORT);
	}

	/**
	 * Run agent controller with the agentControllerServerPort.
	 * 
	 * @param agentControllerServerPort
	 *            agent controller server port
	 */
	public void run(int agentControllerServerPort) {
		run(null, agentControllerServerPort);
	}

	/**
	 * Run agent controller with the given agent controller host and the agent
	 * controller server port.
	 * 
	 * @param agentControllerServerHost
	 *            agent controller server host
	 * @param agentControllerServerPort
	 *            agent controller server port
	 */
	public void run(String agentControllerServerHost, int agentControllerServerPort) {
		if (agentControllerServerHost != null) {
			properties.setProperty(AgentConfig.AGENT_CONTROLER_SERVER_HOST, agentControllerServerHost);
		}
		if (agentControllerServerPort != 0) {
			properties.setInt(AgentConfig.AGENT_CONTROLER_SERVER_PORT, agentControllerServerPort);
		}
		run(properties);
	}

	private long count = 0;

	/**
	 * Run agent controller with given {@link GrinderProperties}. server host
	 * and port will be gained from {@link GrinderProperties}
	 * 
	 * @param grinderProperties
	 *            {@link GrinderProperties}
	 */
	public void run(final GrinderProperties grinderProperties) {
		grinderProperties.put("grinder.region", region);
=======
	private final ListenerSupport<AgentControllerShutDownListener> m_listeners = ListenerHelper.create();
	private boolean forceShutdown = false;
	// event synchronization for
	@SuppressWarnings("FieldCanBeLocal")
	private Condition m_eventSyncCondition = new Condition();

	public static final Logger LOGGER = LoggerFactory.getLogger("agent controller daemon");


	/**
	 * Constructor.
	 *
	 * @param agentConfig agent config
	 */
	public AgentControllerDaemon(AgentConfig agentConfig) {
		try {
			agentController = new AgentController(m_eventSyncCondition, agentConfig);
		} catch (GrinderException e) {
			throw processException("Exception occurred while initiating the agent controller daemon", e);
		}
	}


	private long count = 0;

	public void run() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		thread = new Thread(new Runnable() {
			public void run() {
				do {
					try {
<<<<<<< HEAD
						if (count % LOG_FREQUENCY == 0) {
							LOGGER.info("The agent controller daemon is started.");
						}
						getAgentController().setAgentConfig(
								checkNotNull(agentConfig, "the agent config should be provided "
										+ "before the controller is started"));
						getAgentController().run(grinderProperties, count);

=======
						if (count++ % LOG_FREQUENCY == 0) {
							LOGGER.info("The agent controller daemon is started.");
						}
						getAgentController().run();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
						getListeners().apply(new Informer<AgentControllerShutDownListener>() {
							public void inform(AgentControllerShutDownListener listener) {
								listener.shutdownAgentController();
							}
						});
<<<<<<< HEAD
						count++;
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					} catch (Exception e) {
						LOGGER.info("Agent controller daemon is crashed. {}", e.getMessage());
						LOGGER.debug("The error detail is  ", e);
					}
<<<<<<< HEAD
					if (isForceToshutdown()) {
						setForceToshutdown(false);
						break;
					}
					ThreadUtil.sleep(GrinderConstants.AGENT_CONTROLLER_RETRY_INTERVAL);
=======
					if (isForceShutdown()) {
						setForceShutdown(false);
						break;
					}
					ThreadUtils.sleep(GrinderConstants.AGENT_CONTROLLER_RETRY_INTERVAL);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				} while (true);
			}
		}, "Agent Controller Thread");
		thread.start();
	}

	/**
	 * Agent controller shutdown listener class.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @author JunHo Yoon
	 */
	public interface AgentControllerShutDownListener {
		/**
		 * Method which will be called when agent controller.
		 */
		public void shutdownAgentController();
	}

	public ListenerSupport<AgentControllerShutDownListener> getListeners() {
		return this.m_listeners;
	}

<<<<<<< HEAD
	/**
	 * Add agent controller shutdown listener.
	 * 
	 * @param listener
	 *            listener
	 */
	public void addListener(AgentControllerShutDownListener listener) {
		m_listeners.add(listener);
	}
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad

	/**
	 * Shutdown agent controller.
	 */
	public void shutdown() {
		try {
<<<<<<< HEAD
			setForceToshutdown(true);
			agentController.shutdown();
			if (thread != null) {
				ThreadUtil.stopQuetly(thread, "Agent controller thread was not stopped. Stop by force.");
=======
			setForceShutdown(true);
			agentController.shutdown();
			if (thread != null) {
				ThreadUtils.stopQuietly(thread, "Agent controller thread was not stopped. Stop by force.");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				thread = null;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public AgentController getAgentController() {
		return agentController;
	}

<<<<<<< HEAD
	private boolean isForceToshutdown() {
		return forceToshutdown;
	}

	private synchronized void setForceToshutdown(boolean force) {
		this.forceToshutdown = force;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public void setAgentConfig(AgentConfig agentConfig) {
		this.agentConfig = agentConfig;
	}

=======
	private boolean isForceShutdown() {
		return forceShutdown;
	}

	private synchronized void setForceShutdown(boolean force) {
		this.forceShutdown = force;
	}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
